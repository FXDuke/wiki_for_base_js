Ссылка на статью в основном меню:

[Advanced getter/setter. Задача "computed".](wiki:/Javascript.Ninja.JS101.2017_1/advanced-gettersetter-zadacha-computed)

Целью статьи является изучение механизмов работы getter/setter на примере задачи computed.

Задача *computed* будет решена постепенно, на каждом этапе добавляя функционал и сложность по принципу "идея" - "решение" - "контрпример" . Первые идеи/решения будут напрямую игнорировать некоторые пункты условия задачи, что сделанно намеренно, в то время как последние - раскрывать тонкости общего решения.

Содержание:
----
1. [Постановка задачи Computed](#постановка-задачи-computed)
2. [Введение. Getter/Setter](#введение-gettersetter)
3. [Решение на основе getter](#решение-на-основе-getter)
4. [Копирование исходного объекта в функции createSmartObject.](#копирование-исходного-объекта-в-функции-createsmartobject)
5. [Определение getter/setter в createSmartObject.](#определение-gettersetter-в-createsmartobject)
6. [Вычисление "магического" свойства только при изменении свойств от которых оно зависит.](#вычисление-магического-свойства-только-при-изменении-свойств-от-которых-оно-зависит)
7. [Определение зависимостей от полей объекта при вычислении "магического" свойства.](#определение-зависимостей-от-полей-объекта-при-вычислении-магического-свойства)
8. [Динамическое определение зависимостей от полей объекта при вычислении "магического" свойства. Условия налагаемые на callback-функцию.](#динамическое-определение-зависимостей-от-полей-объекта-при-вычислении-магического-свойства-условия-налагаемые-на-callback-функцию)
9. [Копирование полей с getter/setter. Параметры полей.](#копирование-полей-с-gettersetter-параметры-полей)
0. [Разрешение зависимостей вычисляемых свойств друг от друга.](#разрешение-зависимостей-вычисляемых-свойств-друг-от-друга)
1. [Оптимизация разрешения зависимостей вычисляемых свойств друг от друга.](#оптимизация-разрешения-зависимостей-вычисляемых-свойств-друг-от-друга)
2. [Прототипное наследование и рефакторинг.](#прототипное-наследование-и-рефакторинг)
3. [Глубокое копирование объекта. Присваивание полям значений типа объект.](#глубокое-копирование-объекта-присваивание-полям-значений-типа-объект)
4. [Тестирование.](#тестирование)


Постановка задачи Computed
------

Что такое *"computed"* поля - это поля, которые *"магическим образом"* рассчитываются на основе других. Представим, что у нас есть поле *"Имя"*, поле *"Фамилия"* и поле *"Отчество"*, на основании их мы можем магически рассчитать поле *"ФИО"*. Ваша задача - реализовать это. Ваша реализация будет состоять из двух функций:*createSmartObject*, создающей объект с *"умной"* функциональностью", и *defineComputedField*, которая добавляет в объект умное поле. Внимание: *"умное"* поле должно обновлять свое значение ***тогда и только тогда, когда*** изменяют значения поля, от которых оно зависит. Как выглядит код:

**Уровень 1:**
```javascript
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronimic: 'Olegovich',
    });
    
    defineComputedField(obj, 'fullName', ['name', 'surname', 'patronimic'], (name, surname, patronimic) => {
      return name + ' ' + surname + ' ' + patronimic;
    });
    
    console.log(obj.fullName); // Vasya Ivanov Olegovich
    obj.surname = 'Petrov';
    console.log(obj.fullName); // Vasya Petrov Olegovich
    obj.fullName = 'foo' // error
```

**Уровень 2:**
```javascript
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronimic: 'Olegovich',
    });

    defineComputedField(obj, 'fullName', function (data) {
      return data.name + ' ' + data.surname + ' ' + data.patronimic;
    });
    // Да, от каких полей зависит наш computed вычисляется МАГИЧЕСКИ

    console.log(obj.fullName); // Vasya Ivanov Olegovich
    obj.surname = 'Petrov';
    console.log(obj.fullName); // Vasya Petrov Olegovich
    obj.fullName = 'foo' // error

```


Введение. Getter/Setter
----

Рассмотрим самый простой вид.

К примеру, у объекта есть свойство, доступ к которому нельзя делать напрямую. Доступ можно реализовать через функции объекта.

Пусть задан объект obj со свойством prop.

```
    const obj = {prop: undefined }
```

Прямой доступ к значению свойства может быть нежелателен по ряду причин, которые часто связаны с бизнес-логикой самого проекта.

1. Некоторые значения свойства могут быть недопустимыми. К примеру: возраст не может быть отрицательным, email должен соответствовать шаблону, валюта должна иметь строго заданную дробность, либо значение должно соответствовать только значению из словаря и т.д.
2. Некоторые изменения должны документироваться. 

Существует механизм изменений значения через функции. К примеру, setProp(val), getProp(). 
Например:

```javascript
    const obj = {prop: undefined ,
       getProp: function (){ return this.prop },
       setProp: function (val){ this.prop=val }}
```

При таком способе используются дополнительные сущности, увеличивающие объем кода и вероятность ошибок в нем.

Изящным решением будет использование getter/setter. 

Для начала переименуем свойство prop в *_prop*. В нем будем хранить значение свойства. Негласная договоренность называть технические поля с символа подчеркивания подразумевает, что обращение напрямую к полю запрещено.

```
    const obj = {_prop: undefined }
```

Воспользуемся механизмом добавления свойств в объект через метод defineProperty() объектов Object или Reflect.

Reflect появился не так давно и частично дублирует уже имеющийся функционал объекта Object и некоторых других объектов. Это связано с выстраиванием правильной парадигмы рефлексивного программирования (***кто может просто и доступно его описать милости просим***) в JavaScript. Есть небольшие различия в механизме работы дублируемых методов.

```javascript
    Reflect.defineProperty(
      obj, // объект, в котором создаем новое свойство
      'prop' // имя свойства
      , {
          get() { // дескриптор свойства
            return this._prop;
          },
          set(value) {
            this._prop = value;
          },
        }
      );
```
Объявление свойства *_prop: undefined* можно опустить, а присвоить начальное значение уже потом. Свойство *_prop* создастся автоматически.
```
    obj.prop = 'begin value';
```

Другой способ объявления getter/setter непосредственно в объекте

```javascript
    const obj = {
      get prop(){ return this._prop },
      set prop(val){ this._prop = val }
    }
    
    obj.prop = 1; // ==> obj._prop=1
```

Так как **get prop()** и **set prop(val)** по сути являются обыкновенными функциями,то внутри них можно производить различные действия со значениями объекта как при обращении, так и при присвоении. Дополнительным плюсом является то, что все действия для конечного пользователя данного объекта скрыты и он может их использовать через обычные операторы **=, ==, ===** и т.д.



Решение на основе getter
------

**Идея:**

Так как нам передается функция, которая зависит от свойств объекта, то при обращении к вычисляемому свойству нужно вернуть значение данной функции, передав в качестве аргументов значения указанных полей объекта.

Краткий алгоритм решения:

1. Функция createSmartObject возвращает ссылку на передаваемый ей объект.
2. Функция defineComputedField создает в объекте свойство computedName.
3. В getter-е свойства computedName прописывается callback-функция, которая обращается к свойствам объекта от которых зависит. 


Наименование полей аргументов от которых зависит callback-функция передаются нам в виде массива, а в саму callback-функцию необходимо передать последовательность значений.

То есть:

1. нужно преобразовать массив названий *args* --> массив значений этих полей. Это можно сделать через **map**: ``` args.map(x => this[x])```
2. полученный массив вставить как аргументы в функцию через spread-оператор.

В итоге: ``` clb(...args.map(x => this[x])); ```


**Решение:**
```javascript
    function createSmartObject(obj) {
      return obj;
    }
    
    function defineComputedField(obj, computedName, args, clb) {
      
      Object.defineProperty(obj, computedName, {
        get() {
          return clb(...args.map(x => this[x]));
        },
        set() {
          throw new Error('Assignment to computed property');
        },
        enumerable: true,
        configurable: false, // по умолчани false оставлено для акцента
      });
    
    }
```

Пример успешной работы:
```javascript
    const oldObj = { a: 'a', b: 'b' };
    const obj = createSmartObject(oldObj);
  
    defineComputedField(obj, 'ab', ['a', 'b'], (a, b) => {
      return a + b;
    });
  
    defineComputedField(obj, 'ba', ['a', 'b'], (a, b) => {
      return b + a;
    });
  
    obj.a = 'c';
    console.log(obj.ab); // cb
    console.log(obj.ba); // bc
```
**Контрпример:**

Поля создаются и работают согласно условию. Проверим, когда происходит вызов callback-функций. Добавим вывод сообщения в консоль:
```javascript
    (a, b) => {
      console.log('called'); // вывод на консоль
      return a + b;
    }
```
При вызове получаем:
```javascript
    console.log(obj.ab)
    //called
    //cb
    console.log(obj.ab)
    //called
    //cb
```
То есть, вызов callback-функции происходит при каждом обращении к вычисляемому свойству.

Далее можно попробовать изменить исходный объект:
```javascript
    oldObj.a='modif';
    console.log(obj.ab);
    // called
    // modifb
```
Изменения происходят и в новом объекте, что является недопустимым.


Очевидно, что  условия задачи **не выполняются**: 

1. *Поле должно обновлять свое значение **тогда и только тогда, когда** изменяют значения поля, от которых оно зависит*.
2. Возвращаемый объект функции createSmartObject должен быть новым объектом.


Минусы данного решения с практической точки зрения:

* Поле вычисляется столько раз сколько к нему обращались. Если callback-функция будет тяжеловесной, то каждый ее вызов будет тормозить программу

* Новый объект является всего лишь ссылкой на изначальный, что может привести к непредвиденным последствиям при модификации данных.


Копирование исходного объекта в функции createSmartObject.
-----------
**Исследование:**

Для устранения выявленных выше недостатков необходимо:

1. Произвести клонирование (копирование) исходного объекта.
2. Выполнять callback-функцию только при изменении параметров.

Из принципа, что callback-функция должна выполняться только при изменении параметров объекта вытекает, что каким-то образом это событие должно фиксироваться.  Механизмом фиксации события изменения может служить setter.

Воспользуемся простым примером для иллюстрации. 

К примеру, есть объект, у которого есть свойство prop. 

```
    const obj = {
      prop: undefined,}
```

Создадим методы get/set для обеспечения доступа к prop.

```javascript
      Object.defineProperty(obj, accessToProp, {
        get() { return this.prop },
        set(val) { 
          console.log('called'); // при присвоении значения на консоль выведется сообщение called 
          this.prop = val 
          }
      });
    
    obj.prop = 1; // called
```

Следовательно для всех полей нового объекта нужно создать getter/setter. 

Сразу возникает несколько сопутствующих проблем:

1. Пользователь ожидает, что все поля будут называться так как они назывались. И обращаться он будет к ним. Следовательно и изменять он будет их, а если мы создадим новые поля с getter/setter, то это ни к чему не приведет.
2. Какими бы наименованиями  функций не пользовались вероятность того, что можно случайно создать дублирующее поле все же существует. Каждый раз делать подобную проверку чрезвычайно затратно.

Выходом из данной ситуации может стать следующее решение:

1. Создание нового объекта(к примеру, *nObj*), в котором есть вложенный объект *_data* (название не принципиально, его можно сделать длиннее, но самое главное, что проверять нужно будет только одно название - *"_data"*)
2. Все значения/названия полей исходного объекта дублируется в *_data*.
3. К корне нового объекта создается getter/setter на поля в объекте *_data*



``` 
          ---- prop1  // get/set on _data.prop1
          | -- prop2  // get/set on _data.prop2
    nObj--|..........
          | -- propN  // get/set on _data.propN
          | -- computedF  // get/set on _data.computedF 
          ---- _data | -- prop1 //original data
                     | -- prop2 //original data
                     |..........
                     | -- propN //original data
                     ---- computedF //original data
    
```


Тогда в setter можно поместить операцию вычисления нового значения "магического" поля. А хранить значение в объекте *_data*. (на диаграмме computedF)


**Идея:**

1. Функция *createSmartObject* создает новый объект с копией исходного  в подобъекте *_data*
3. Функция *defineComputedField* создает *getter/setter* в корне передаваемого объекта для всех полей в *_data*. 
4. При вызове setter выполняется callback-функция и ее результат записывается в *_data[computedName]*
4. *Getter* поля *computedName* выполняет обращение к *_data[computedName]*, а setter выводит ошибку:


Обратить внимание:

1. Проверка существования поля с такми же названием можно сделать с помощью метода  *Reflect.has(obj, fieldName)*
2. Примечательно, что вызов callback-функции можно сделать как *clb(...args.map(x => this._data[x]))* так и *clb(...args.map(x => this[x]));*. Введением getter/setter задается формат взаимодействия с полем. Предпочтительно его придерживаться и в самой разработке, поэтому будем использовать clb(...args.map(x => this[x]));


**Решение:**

```javascript
    function createSmartObject(obj) {
      if (Reflect.has(obj, '_data')) throw new Error('Property "_data" already exists.');

      const newObj = {_data:{}};
      Object.keys(obj).forEach(key => {
        newObj._data[key] = obj[key]
      })
      return newObj;
    }
    
    function defineComputedField(obj, computedName, args, clb) {
      if (Reflect.has(obj._data, computedName)) throw new Error('Property "'+computedName+'" already exists.');

      Object.keys(obj._data).forEach(key => {
        
        Object.defineProperty(obj, key, {
          get() {
            return this._data[key];          
          },
          set(val) {
            this._data[key] = val;
            this._data[computedName] = clb(...args.map(x => this[x]));
          },
          enumerable: true,
        });

      })

      Object.defineProperty(obj, computedName, {
        get() {
          return this._data[computedName];
        },
        set() {
          throw new Error('Assignment to computed property');
        },
        enumerable: true,
      });

    } // end of defineComputedField
```

Проверим работу простым тестом:
```javascript
    const oldObj = { a: "a", b: "b" , c:"c"};
    const obj = createSmartObject(oldObj);

    defineComputedField(obj, "ab", ["a", "b"], (a, b) => {
      console.log("called");
      return a + b;
    });

    console.log(obj.ab); // undefined
```

Результат не тот, который мы ожидали.
Дело в том, что функция *defineComputedField* только создает поля, поэтому callback-функция не была вызвана ни разу (в консоли не видим 'called') . 
Добавим перед завершающей скобкой еще строчку кода

```javascript
       obj._data[computedName] = clb(...args.map(x => obj[x]));
    } // end of defineComputedField
```

повторим тесты:
    
```javascript 
      .............
    // called
    console.log(obj.ab); // ab
    obj.a = 'test';
    // called
    console.log(obj.ab) // testb
    
    oldObj.a='modif';
    console.log(obj.a) // 'test'

```
Решено несколько задач:

1. Callback-функция вызывается только тогда, когда изменяются поля объекта.
2. Данные исходного объекта не влияют на данные нового.

**Контрпример:**

К сожалению, у нового решения есть ряд недостатков. Пожалуй самым существенным является то, что можно создавать только одно вычисляемое свойство.

При попытке создать новое свойство получим ошибку:
```javascript
      defineComputedField(obj, "ba", ["a", "b"], (a, b) => {
        return b + a;
      });

      // Uncaught TypeError: Cannot redefine property: a
          // at Function.defineProperty (<anonymous>)
          // at Object.keys.forEach.key 
          // at Array.forEach (<anonymous>)
          // at defineComputedField 
```

Дело в том, что getter/setter для свойств объекта *_data* создаются в  defineComputedField и повторное их определение уже невозможно. Если их удалять и создавать новые, то это разрушает работу старых.


Определение getter/setter в createSmartObject.
-----------

**Исследование:**

Проблема заключается в том, что если единожды определять getter/setter для свойств в *_data* нового объекта, то это логично сделать в createSmartObject, но ведь callback-функция, для которой это делается, появляется только в функции defineComputedField.

Следовательно, необходимо разработать механизм, благодаря которому  новый объект сможет оперировать callback-функциями.

Так как в JS функции являются объектами первого класса, то их можно передавать в качестве аргумента. Передать ее непосредственно в createSmartObject в качестве аргумента нельзя, так как callback-функция объявляется позже. 

Значит нужно создать какой-то интерфейс между createSmartObject и функцией defineComputedField. В свою очередь, в createSmartObject каждый setter из этого интерфейса будет каждый раз вызывать callback-функцию.

Еще одим свойством функций первого класса является то, что можно их присваивать переменным.

Пусть в новом объекте будет служебная переменная, которая будет вызываться каждый раз, когда вызывается setter нового объекта.


Проверим догадку простым примером:

```javascript
    const obj = {
      get prop() { return this._prop; },
      set prop(val) { 
        obj.func();
        this._prop = val;
      },
      func: () => {}
    };

    obj.func = () => {console.log("called");};
    obj.prop = "test"; // called
```

Однако если сразу воспользоваться данной идеей, то ни к чему новому это не приведет так как служебная переменная одна, добавлении второго умного поля приведет к затиранию callback-функции предыдущего вызова defineComputedField.

Значит, нужно заменить служебную переменную на массив, и уже в него передавать callback-функции с последующей их обработкой внутри setter.

простой пример:

```javascript
    const obj = {
      get prop() {
        return this._prop;
      },
      set prop(val) {
        obj.funcs.forEach(x => x());
        this._prop = val;
      },
      funcs: []
    };

    obj.funcs.push(() => {
      console.log("called");
    });
    obj.prop = "test"; // called
```

Когда выше говорилось про передачу callback-функций в массив, то делалось обобщение, потому что простая передача функции не совсем корректна. Setter не знает от каких параметров зависит callback-функция, какие аргументы стоит в нее подставлять. 

Есть два пути решения данной проблемы:

1. Расширить поля массива и передавать с ним массив наименований полей от которых зависит функция, а так же поле в которое нужно будет записать новое значение.
2. Обернуть callback функцию в другую, которая подставит все необходимые значения, а в setter будет только вызов обернутой функции.

Этап исследования завершен можно сформулировать общую идею решения.


**Идея:**

1. Функция *createSmartObject* создает новый объект с копией исходного в подобъекте *_data*.
2. Параллельно создаются getter/setter свойств, ссылающиеся на поля в *_data*.
3. В *setter* прописывается алгоритм перебора массива с функциями с последующим их исполнением.
4. Функция *defineComputedField* оборачивает callback-функцию так, что бы ее можно было вызвать без параметров. Добавляет новую функцию в массив функций нового объекта.
5. *Getter* поля *computedName* выполняет обращение к *_data[computedName]*, а *setter* выводит ошибку.


**Решение:**

Обратить внимание:

1. Необходимо отметить, что мы передаем wrapClb, которая зависит от *obj*, в свою очередь, который находится в контексте createSmartObject, а будет выполняться в контексте *defineComputedField*. Это работает благодаря механизму замыкания в JS.



```javascript
    function createSmartObject(obj) {
      if (Reflect.has(obj, "_data"))
        throw new Error('Property "_data" already exists.');
      if (Reflect.has(obj, "_arrFunc"))
        throw new Error('Property "_arrFunc" already exists.');
    
      const newObj = { _data: {}, _arrFunc: [] };
      Object.keys(obj).forEach(key => {
        newObj._data[key] = obj[key];
    
        Object.defineProperty(newObj, key, {
          get() {
            return this._data[key];
          },
          set(val) {
            this._data[key] = val;
            newObj._arrFunc.forEach(f => f());
          },
        });
      });
      return newObj;
    }
    
    function defineComputedField(obj, computedName, args, clb) {
      if (Reflect.has(obj._data, computedName))
        throw new Error('Property "' + computedName + '" already exists.');
    
      const wrapClb = () => {
        obj._data[computedName] = clb(...args.map(x => obj[x]));
      };
    
      obj._arrFunc.push(wrapClb);
    
      Object.defineProperty(obj, computedName, {
        get() {
          return this._data[computedName];
        },
        set() {
          throw new Error("Assignment to computed property");
        },
      });
    
      wrapClb(); // первый вызов callback-функции
    } // end of defineComputedField
```

Проверим работу простым тестом:
```javascript
    const oldObj = { a: "a", b: "b", c: "c" };
    
    const obj = createSmartObject(oldObj);
    
    defineComputedField(obj, "ab", ["a", "b"], (a, b) => {
      console.log("called ab");
      return a + b;
    });
    
    defineComputedField(obj, "ba", ["a", "b"], (a, b) => {
      console.log("called ba");
      return b + a;
    });
    
    obj.a = "c";
    console.log(obj.ab); // cb
    console.log(obj.ba); // bc
```

Стоит отметить:

1. Обращение к вычисляемым полям не вызывает callback-функцию.
2. Новые вычисляемые поля не влияют на работу друг друга.


**Контрпример:**

в вышеуказанном примере произведем присвоение полю от которого вычисляемые свойства не зависят
```javascript
    obj.c = "test";
    // called ab
    // called ba
```

Дело в том, что на любое изменение, а по сути любой вызов setter автоматически запускается пересчет всех callback-функций, что противоречит изначальному условию:

*Внимание: *"умное"* поле должно обновлять свое значение ***тогда и только тогда, когда*** изменяют значения поля, от которых оно зависит.*

Вычисление "магического" свойства только при изменении свойств от которых оно зависит.
----------------

**Исследование:**

Имена полей, от которых зависит вычисляемое свойство, передаются функцией defineComputedField при объявлении этого свойства. У нас уже есть связь между defineComputedField и createSmartObject через массив. 


**Идея:**

1. Передать поля, от которых зависит вычисляемое поле? через массив вместе с обернутой callback-функцией в виде объекта.
2. В запуске *setter* инициировать цикл проверки массива с информации из п.1.
3. Если в массиве args есть значение совпадающие с название поля *setter* запустить обернутую callback-функцию.


**Решение**

Обратить внимание:

* Если раньше мы передавали только функцию, то теперь будем передавать объект, в котором есть массив аргументов и функция 
```javascript 
      obj._arrFunc.push({ wrapClb, args }); 
```
при этом используем синтаксис ES6, то есть выражение выше эквивалентно следующему выражению:
```javascript 
      obj._arrFunc.push({ wrapClb: wrapClb, args: args }); 
```

* Необходимо подробнее остановиться на том, что происходит в этом выражении:
```javascript 
      newObj._arrFunc.forEach(wargs => {
        if (wargs.args.some(arg => arg === key )) wargs.wrapClb();
      });
```
Если по пунктам:

1. Для каждого элемента, который будет представлен переменной *wargs* массива *_arrFunc* объекта *newObj*, выполняется проверка:
1. Если внутри массива *args* существуют поля, которые равны *key* (полю создаваемого нового свойства), а по сути названию поля с getter/setter, то далее:
1. Выполнить функцию *wargs.wrapClb*.



Листинг:

```javascript
    function createSmartObject(obj) {
      if (Reflect.has(obj, "_data"))
        throw new Error('Property "_data" already exists.');
      if (Reflect.has(obj, "_arrFunc"))
        throw new Error('Property "_arrFunc" already exists.');
    
      const newObj = { _data: {}, _arrFunc: [] };
      Object.keys(obj).forEach(key => {
        newObj._data[key] = obj[key];
    
        Object.defineProperty(newObj, key, {
          get() {
            return this._data[key];
          },
              set(val) {
                this._data[key] = val;
                newObj._arrFunc.forEach(wargs => {
                if (wargs.args.some(arg => arg === key )) 
                     wargs.wrapClb();
            });
          },
        });
      });
      return newObj;
    }
    
    function defineComputedField(obj, computedName, args, clb) {
      if (Reflect.has(obj._data, computedName))
        throw new Error('Property "' + computedName + '" already exists.');
    
      const wrapClb = () => {
        obj._data[computedName] = clb(...args.map(x => obj[x]));
      };
    
      obj._arrFunc.push({ wrapClb, args });  
    
      Object.defineProperty(obj, computedName, {
        get() {
          return this._data[computedName];
        },
        set() {
          throw new Error("Assignment to computed property");
        },
      });
    
      wrapClb(); // первый вызов callback-функции
    } // end of defineComputedField
```

Выполним тесты:

```javascript
      const oldObj = { a: "a", b: "b", c: "c" };
      
      const obj = createSmartObject(oldObj);
      
      defineComputedField(obj, "ab", ["a", "b"], (a, b) => {
        console.log("called ab");
        return a + b;
      });
      //called ab
      
      defineComputedField(obj, "ba", ["a", "b"], (a, b) => {
        console.log("called ba");
        return b + a;
      });
      //called ba

      obj.a = "c";
      //called ab
      //called ba

      console.log(obj.ab); // cb
      console.log(obj.ba); // bc
      obj.c = "test";
      //...
      // nothing happens

```

**Контрпример:**

Пусть в исходном объекте будет поле с getter/setter

```javascript
const oldObj = {
  a: "a",
  b: "b",
  c: "t",
  get g() {
    return this._g + this.c;
  },
  set g(val) {
    this._g = val;
  }
};

oldObj.g = "g";

const obj = createSmartObject(oldObj);

defineComputedField(obj, "abg", ["a", "b", "g"], (a, b, g) => {
  console.log("called abg");
  return a + b + g;
});

//called abg

obj.a = "c"; //called abg
obj.c = "test"; // nothing happens!!!
console.log(obj.abg); //cbgt

```

Если посмотреть на структуру нового объекта, то мы увидим, что getter/setter не скопировались, а если точнее сказать, то скопировались возвращаемые ими значения при копировании.

Стоит отметить, что если даже будут корректно скопированы getter/setter так, что они будут активными, то они могут зависеть от совершенно других полей нежели чем callback-функция. Следовательно, теперь необходимо ориентироваться не на входящий массив args, а вычислять зависимости динамически. А это уже задача **второго** уровня сложности.



Определение зависимостей от полей объекта при вычислении "магического" свойства.
----------------
**Уточнение задания:**

Вид defineComputedField для второго уровня выглядит следующим образом
```
    defineComputedField(obj, computedName, function (data) { <CODE>});
```
Разберем callback-функцию подробно:
```
    function (data) { <CODE>}
```

Смысл аргумента *data* в том, чтобы внутри callback-функции было обращение к полям исходного объекта, то есть obj. Если перефразировать, то это и есть объект obj. 

Во время выполнения программы должно произойти примерно следующее:
```
      // пояснение:
      // obj - "магический" объект
      // clb = function (data) { <CODE> } // callback-функция 
      // clb - это аргумент функции defineComputedField
      // obj._data - клон исходного объекта
      ...
      obj._data[computedName] = clb(obj); //
      ...
```
в свою очередь обращение к *computedName* будет происходить через getter.


**Исследование:**

Необходимо понять, как определить, от каких свойств зависит функция.
Представим, что исходный объект имеет набор свойств:
```
    obj=---- prop1
        |--- prop2
        |--- ...
        |--- propN
```
Он подается на вход clb-функции, а она, в свою очередь, возвращает значение, которое записывается в хранимое поле вычисляемого свойства.
```
    obj->clb->obj._data[computedName]
```
 В общем случае функция зависит не от всех свойств объекта, а только от некоторых:

```
        |--- prop1------|----------|
        |--- prop2      |          |
    obj-|--- prop3------| clb()    |--->obj._data[computedName]
        |--- ...        |          |
        |--- propN------|          |
        |--- propN+1    |----------|
```

При выполнении callback-функции происходит обращение к методам get этих свойств объекта. Как показано на схеме, это будут поля:
```
    prop1
    prop2
    propN 
```

Так как уже создан объект, в котором можно отслеживать обращение к его свойствам через getter, то эти обращения можно фиксировать. 
После выполнения callback-функции уже можно сказать, от каких свойств она зависит.

Приведем простой пример:
```javascript
    const o = { get a(){ console.log('called a'); return this._a },
                get b(){ console.log('called b'); return this._b },
                get c(){ console.log('called c'); return this._c },
                _a: 1, _b: 1, _c: 1};
    o.a === o.b; // данный код необходим что бы показать, что идет обращение
    // called a
    // called b
```

Сallback-функция будет выполнена **как минимум один раз**: в функции defineComputedField перед ее самым завершением, то есть при инициализации obj._data[computedName]. Это условие обеспечивает то, что мы сможем отслеживать зависимость callback-функции от свойств объекта уже при выполнении defineComputedField.


Далее определимся, где хранить и как идентифицировать "отловленные" обращения, поскольку нам нужно отследить эти обращение и точно знать, что они были инициированы определенной callback-фунцией.

Здесь необходимо воспользоваться следующими приёмами:

1. механизмом замыкания 
2. массивом для хранения данных

*Тонкие моменты:*

1. После того как заполнение массива данными будет настроено, оно будет выполняться при каждом вызове getter-а. К примеру, был создан smart-объект и его уже начали использовать, а только потом объявили вычисляемое поле. Получается, данный массив уже будет заполнен ненужными элементами. **Выходом** может быть установка флага, который бы включал и выключал заполнение данного массива аргументов.
2. Заведение нового массива добавит новую сущность, хотя уже есть массив, где хранилось подобная сущность - args. Поэтому стоит заполнять именно его.
3. Еще целесообразно использовать не массив, потому что функция для вычисляемого поля может делать не одно обращение к какому-то конкретному полю в объекте. Чтобы не заполнять массив дублирующими значениями, стоит использовать Set. (Хотя можно видеть, что в нижеследующем решении это не принципиально, потому что вместо has использовалось бы some и результат был бы тем же. Данный механизм является "хорошим тоном" и экономией ресурсов. Кроме того, он облегчает контроль массива аргументов при отладке.)


**Идея:**

1. Ввести служебное поле, которое включало бы запись и отключало бы ее.
2. При обращении к свойству при включенном флаге заполнять массив аргументов именами вызванных свойств.
2. В функции defineComputedField перед определением вычисляемого поля объекта включать флаг записи getter.


**Решение:**

Особенности решения:

1. Упрощается вызов callback-функции: *obj._data[computedName] = clb(obj);*
2. Необходимо подробно остановиться на строчке кода ``` obj._arrFunc.push({ wrapClb, args: new Set }); ``` . Создается запись в массиве с обернутой callback-функцией. В множество *args* будут добавляться наименование полей
3. В строке ``` if (this._flag) newObj._arrFunc[newObj._arrFunc.length - 1].args.add(key); ``` если запись включена, то обращаемся к последней записи массива обернутых функций и добавляем в множеcтво еще одно поле.


Листинг:

```javascript
    function createSmartObject(obj) {
      if (Reflect.has(obj, "_data"))
        throw new Error('Property "_data" already exists.');
      if (Reflect.has(obj, "_arrFunc"))
        throw new Error('Property "_arrFunc" already exists.');
    
      const newObj = { _data: {}, _arrFunc: [], _flag: false };
      Object.keys(obj).forEach(key => {
        newObj._data[key] = obj[key];
    
        Object.defineProperty(newObj, key, {
          get() {
            if (this._flag) 
              newObj._arrFunc[newObj._arrFunc.length - 1].args.add(key);
            return this._data[key];
          },
              set(val) {
                this._data[key] = val;
                newObj._arrFunc.forEach(wargs => {
                if (wargs.args.has(key)) 
                     wargs.wrapClb();
            });
          },
        });
      });
      return newObj;
    }
    
    function defineComputedField(obj, computedName, clb) {
      if (Reflect.has(obj._data, computedName))
        throw new Error('Property "' + computedName + '" already exists.');
    
      const wrapClb = () => {
        obj._data[computedName] = clb(obj);
      };  
       
    
      Object.defineProperty(obj, computedName, {
        get() {
          return this._data[computedName];
        },
        set() {
          throw new Error("Assignment to computed property");
        },
      });

      obj._flag = true; // включение записи getter
      obj._arrFunc.push({ wrapClb, args: new Set }); 
      wrapClb(); // первый вызов callback-функции
      obj._flag = false; // выключение записи getter
    } // end of defineComputedField
```

проверим работоспособность простыми тестами:

```javascript
    const oldObj = {
      a: "a",
      b: "b",
      c: "t",
      ifProp: 0
    };

    const obj = createSmartObject(oldObj);
    
    obj.c='t1';

    defineComputedField(obj, "ab", data => {
      console.log("called ab");
      return data.a + data.b;
    });
    // called ab

    obj.c='t2';
    
    defineComputedField(obj, "ba", data => {
      console.log("called ba");
      return data.b + data.a;
    });
    // called ba
    obj.c='t3';

    obj.a = "m";
    // called ab
    // called ba
    
    obj.c = "test";
    console.log(obj.ab); //mb
    console.log(obj.ba); //mb
```

Итоги: 

1. Создаются новые свойства, которые не влияют друг на друга
2. Процесс записи идет в точности как мы предполагали. Благодаря замыканием непредвиденных добавлений в массив *args* быть не может 
3. Немного смущает прямой доступ к последнему элементу массива, что затрудняет чтение кода ``` newObj._arrFunc[newObj._arrFunc.length - 1].args.add(key); ```


**Контрпример**

Проведем новый тест:

```javascript
    defineComputedField(obj, "ifTest", data => {
      console.log("called ifTest");
      if (data.ifProp === 0) return data.a;
      return data.b;
    });
    // called ifTest
    
    obj.a = "test1";
    // called ifTest
    
    console.log(obj.ifTest); // test1
    // все работает как надо
    
    obj.ifProp = 1;
    // called ifTest
    
    obj.b = "test2";
    // нет вызова ifTest !!!
    
    console.log(obj.ifTest); // b 
    // свойство не изменилось!!!
```

Дело в том, зависимости были определены один раз при объявлении метода. Следовательно, необходимо динамически менять зависимости при каждом вызове callback-функции. 


Динамическое определение зависимостей от полей объекта при вычислении "магического" свойства. Условия, налагаемые на callback-функцию.
--------------------

**Исследование:**

Пользуясь вышеуказанным примером, можно схематично показать принцип работы callback-функции с изменяющимися зависимостями. 

```
        |--- prop1------|--------------|
        |--- prop2      | clb()        |
    obj-|--- prop3------| --------     |
        |--- prop4------|if F(props):  |--->obj._data[computedName]
        |--- prop5------|   get prop4  |
        |--- ...        |else          |
        |--- ...        |   get prop5  |
        |--- propN------|              |
        |--- propN+1    |--------------|
```

Существует какая-то зависимость, которую можно указать как функцию от входящих аргументов, то есть F(props), от которой зависит набор параметров, к которым происходит обращение.

**ВАЖНО!!!** Функция *F* должна зависеть **ТОЛЬКО** от свойств объекта. Это есть ограничение как на саму функцию, так и на clb.
В противном случае задача динамического определения не может быть решена.

Таким примером может быть следующая callback-функция
```
      function (data) {
        if (parseInt(Math.random() * 9) % 2 === 0) return data.a;
        return data.b;
      }
```

В данном примере callback функция зависит от случайной величины и обращение к полям data.a и data.b не зависит от параметров объекта.

**Будем считать, что функций такого рода не будет! Это ограничение на callback-функцию.**


Перед тем как начать программировать, необходимо провести мысленный эксперимент.

Допустим, что после первого вызова callback-функции она обратилась к следующим getter:

```
      prop1
      prop2
      prop3
      ..
      propK
      ...
      propN
```

Стоит задаться вопросом, важно ли знать, какой из этих параметров участвует в возможно существующей операции выбора свойств. Ответ **нет**.


Нам **важно** знать только то, что было обращение к ним, а значит от них зависит поведение callback-функции в данный момент - именно эти параметры сохраняют ее поведение и текущее состояние.

То есть, если хоть один из этих параметров изменится, нам необходимо пересчитать вычисляемое свойство. 

Допустим, изменился параметр ``` propK ```, и при вызове callback-функции мы наблюдаем обращение к следующим свойствам:

```
      prop2
      prop3
      ...
      propN+1
```

Теперь уже эти новые свойства будут являться свойствами, от которых зависит callback-функция.

Можно сформулировать правило для callback-функции: каждому состоянию объекта должно однозначно соответствовать значение callback-функции.
То есть, если вызвать функцию с одним и тем же состоянием объекта, то получим один и тот же результат.


Можно построить следующую цепочку:

```
            -----------           ----------- 
            | get call |          | get call| 
            | ---------|          | --------| 
            |   prop1  |          |         | 
   clb call |   prop2  |-- set  ->| prop2   | -- set  --> nothing 
            |   ...    |  propK   | ...     |   prop1     happens 
            |   propK  |          | propK   | 
            |   ...    |          | ...     | 
            |   propN  |          | propN+1 | 
            ------------          ------------ 
         
```

Почему ничего не должно происходить, когда изменяется параметр prop1?
Потому что теперь объект изменился и prop1 не входит в то множество, от которого зависит значение вычисляемого поля. Теперь оно зависит от других.


Технология изменения зависимостей ясна. Теперь необходимо реализовать правильный их учет.

Требования:

Список свойств, от которых зависит вычисляемое поле, должен изменяться после каждого вызова callback-функции и храниться до следующего ее вызова.

Введем понятия:

1. Массив-ловушка - это массив сбора имен свойств, передаваемых геттерами smart-объекта.
2. Массив соответствия - это массив соответствия зависимого поля и обернутой callback-функции.


**Идея:**

1. Настроить getter-ы smart-объекта так, чтобы они сбрасывали имена своих полей в специальный массив-ловушку. В результате чего при каждом чтении этих полей их имена будут записаны в ловушке.
2. Перед вызовом callback-функции со smart-объектом в качестве аргумента массив-ловушку очищаем.
3. В процессе выполнения функция обратится ко всем нужным ей аргументам, каждый из которых в свою очередь запишет свое имя в массив-ловушку.
4. В результате, ловушка будет содержать имена всех необходимых полей.
5. Удаление из массива соответствия всех упоминаний о данной callback-функции (либо ее обертки в зависимости от алгоритма).
5. Передача из массива-ловушки в массив соответствия имени поля, от которого зависит callback-функция, и самой обернутой callback-функции.
**Замечание**: для упрощения реализации в качестве ловушки лучше взять не стандартный Array, а объект Set, чтобы не беспокоиться о продублированных именах


**Решение:**


Обратить внимание:

Данный листинг немного сложнее в плане построения связей.

* объявление массива функций ``` const toDoFunc = []; ``` обусловлено тем, что если сразу вызывать обернутые функции, то можно вызвать бесконечную рекурсию. 
* обертывание callback-функции стоит рассмотреть подробно
```javascript
    const wrapClb = () => {
      obj._getterCatch.clear(); // очишаем массив ловушку
      obj._data[computedName] = clb(obj); // присваиваем вычисляемое поле
      obj._setterMem.forEach(wp => { 
        if (wp.wrapClb === wrapClb) // удаляем из массива соответсвия все флаги связанные с обернутой callback-функцией
          obj._setterMem.delete(wp); // удаление
         });
      obj._getterCatch.forEach(key => obj._setterMem.add({ key, wrapClb }));
      // запись в массив соответствия обернутой callback-функции и поля

        };
```        
* Тонкость заключается в том, что мы создаем обернутую callback - функцию, которая сама себя записывает в массив-соответствия и проверяет себя же в нем.
То есть: "Есть ли я в массиве вызова? Если да, то удалить, а потом записать меня в массив соответствия уже с зависимостями от новых свойств".

Листинг:

```javascript
      function createSmartObject(obj) {
        if (Reflect.has(obj, "_data"))
          throw new Error('Property "_data" already exists.');
        if (Reflect.has(obj, "_arrFunc"))
          throw new Error('Property "_arrFunc" already exists.');
      
        const newObj = { _data: {}, _getterCatch: new Set(), _setterMem: new Set() };
      
        Object.keys(obj).forEach(key => {
          newObj._data[key] = obj[key];
      
          Object.defineProperty(newObj, key, {
            get() {
              this._getterCatch.add(key);
              return this._data[key];
            },
            set(val) {
              this._data[key] = val;
              const toDoFunc = [];
              this._setterMem.forEach(sCatch => {
                if (sCatch.key === key) {
                  toDoFunc.push(sCatch.wrapClb);
                }
              });
              toDoFunc.forEach(f => f());
            },
          });
        });
      
        return newObj;
      }


      function defineComputedField(obj, computedName, clb) {
        if (Reflect.has(obj._data, computedName))
          throw new Error('Property "' + computedName + '" already exists.');
      
        const wrapClb = () => {
          obj._getterCatch.clear();
          obj._data[computedName] = clb(obj);
          obj._setterMem.forEach(wp => {
            if (wp.wrapClb === wrapClb) obj._setterMem.delete(wp);
          });
          obj._getterCatch.forEach(key => obj._setterMem.add({ key, wrapClb }));
        };
      
        Object.defineProperty(obj, computedName, {
          get() {
            return this._data[computedName];
          },
          set() {
            throw new Error("Assignment to computed property");
          },
        });
      
        wrapClb(); // первый вызов callback-функции
      } // end of defineComputedField
```

Проверим тестом:

```javascript
    const oldObj = {
      a: "a",
      b: "b",
      c: "t",
      ifProp: 0
    };
    
    const obj = createSmartObject(oldObj);
    
    defineComputedField(obj, "ifTest", data => {
      console.log("called ifTest");
      if (data.ifProp === 0) return data.a;
      return data.b;
    });
    // called ifTest
    console.log(obj.ifTest); // a
    obj.ifProp = 1;
    // called ifTest
    obj.b = "test";
    // called ifTest
    console.log(obj.ifTest); // test
``` 

Итог:
Динамическое отслеживание зависимостей работает.

**Контрпример:**

Пример 1:

```javascript
    const oldObj = {
      a: "a",
      b: "b",
      c: " !NothingHappend! ",
      get g() {
        return this._g + this.c;
      },
      set g(val) {
        this._g = val;
      }
    };
    
    oldObj.g = "g";
    
    const obj = createSmartObject(oldObj);
    
    defineComputedField(obj, "abg", data => {
      console.log("called abg");
      return data.a + data.b + data.g;
    });
    // called abg
    
    obj.a = "c";
    // called abg
    obj.c = "good job";
    console.log(obj.abg); // cbg !NothingHappend! 
```

Дело в том, что мы не копировали setter/getter, что и привело к тому, что изменение *obj.c* не запускает пересчет всех зависимостей.

Пример 2:

```javascript
Object.defineProperty(oldObj,'hidden',{
  value: 1,
  writable: true,
  enumerable: false,
  configurable: true,
});
oldObj.hidden='test';
console.log(oldObj.hidden); // test
........

obj.hidden; // undefined 
```
Свойство hidden скрыто для определенных методов, но это не значит, что его можно игнорировать.

Копирование полей с getter/setter. Параметры полей.
----------

**Введение:**

Перед тем как перейти к решению задания, необходимо рассмотреть механизм объявления свойств в объекте. 

К примеру, есть статический метод для получения имен полей объекта:
```javascript
      Object.keys()
```
Однако если мы попробуем перебрать все данные массива, то обнаружим, что там нет такого свойства, как length. Обращаться к нему мы можем, можем его изменять, но в переборе свойств оно отсутствует.

При определении нового свойства объекта можно указать следующие параметры:

```javascript 
    Object.defineProperty(obj,'propName',{
      value: 50, // значение
      
      writable: true, //можно ли перезаписывать, то есть изменять значение // по умолчанию false
    
      enumerable: true, // можно ли его получить с помощью различных методов перебора или к нему можно обратиться только зная его имя. // по умолчанию false
      
      configurable: true, // можно ли изменить вышеупомянутые свойства во время работы  // по умолчанию false
})
``` 

Соответственно, "играя" с этими параметрами, можно изменять поведения свойства объекта. В примерах иногда задано значение false для writable, enumerable или configurable, это сделанно намеренно, хотя если параметр не указан, то он будет false.

```javascript 
const obj = {};
Object.defineProperty(obj,'hidden',{
  value: 1,
  writable: true,
  enumerable: false,
  configurable: true,
});

Object.keys(obj); // []
```

То есть мы указали enumerable = false, и поэтому данное свойство не попало в выборку. Если в исходном объекте существует такое поле, то при простом копировании через Object.keys(a) оно не попадет в новый объект.

```javascript 
    const obj = {};
    Object.defineProperty(obj,'readOnly',{
      value: 1,
      writable: true,
      enumerable: true,
      configurable: true,
    });
    
    obj.readOnly = 10;
    console.log(obj.readOnly) //10
```

При этом ошибки не будет.

Однако если поставить флаг строгого режима, то интерпретатор выдаст ошибку.

```javascript 
    'use strict'; // флаг для среды выполнения, что необходимо использовать строгий режим 

    const obj = {};
    Object.defineProperty(obj,'readOnly',{
      value: 1,
      writable: true,
      enumerable: true,
      configurable: true,
    });
    
    obj.readOnly = 10;
     //Uncaught TypeError: Cannot assign to read only property 'readOnly' of object '#<Object>'
```
Дополнительно стоит отметить, что этот флаг является обычной строкой.
Строгий режим применяется ко всему скрипту или к отдельным функциям, но не может быть применён к блокам операторов.

**Исследование:**

Исходя из вышесказанного, для перебора всех свойств объекта нам не подходит обычный метод Object.keys().

Воспользуемся расширенным его аналогом.
``` javascript
        Object.getOwnPropertyNames();
```
Изменим предыдущий пример:

```javascript 
    const obj = {};
    Object.defineProperty(obj,'hidden',{
      value: 1,
      writable: true,
      enumerable: false,
      configurable: true,
    });
    
    Object.getOwnPropertyNames(obj); // ["hidden"]
```
Это вполне подходит для нашей задачи.

Теперь нужно скопировать getter/setter какого-то поля. В первую очередь необходимо понять, что это всего лишь функции. То есть однозначного копирования все равно не получится. Но почему, даже если это будут ссылки, они будут работать?

Попробуем разобраться в работе getter/setter:

```javascript
    const obj = {
      get g() {
        return this._g + this.c;
      },
      set g(val) {
        this._g = val;
      }
    };
```

Особое место здесь занимает ключевое слово **this**. Оно много раз использовалось, но описание пропускалось. Здесь **this** привязан к объекту, свойство которого сейчас описано. Необходимо читать, что this === obj.

Проверим это простым примером
```javascript
    const obj = {
      get g() {
        return this === obj;
      },
    };
    console.log(obj.g); // true
```

Почему нежелательно использовать obj вместо this?

Немного перепишем наш пример для того, что бы не забегать вперед.
```javascript
    const obj = {
      g: function () {
        return this === obj;
      },
    };
    console.log(obj.g()); // true
```
Его логика и поведение абсолютно такие же, только мы используем простую функцию вместо getter.

Создадим новый объект с двумя функциями, и передадим их по ссылке:
```javascript
    const obj = {
      val: 1,
      g: function () {
        return this === obj;
      },
      getVal: function () {
        return this.val;
      },
    };
    
    const obj2 = {val:2};
    obj2.g = obj.g;
    obj2.getVal = obj.getVal;
    console.log(obj2.g()); // false
    console.log(obj2.getVal()); // 2
```

Самое важное в этом примере
1. В функции *g()* **this** в новом объекте стал другим, а если точнее то obj2.
2. Функция getVal стала работать в новом объекте, но уже с его данными (в результате, вернулось 2).

По сути методы и геттеры/сеттеры, созданные в исходном объекте, при правильно копировании (по ссылке) и при правильном изначальном проектировании объекта могут быть перенесены в новый smart-объект.

Посмотрим как это можно сделать.

Для этого необходимо изучить еще один метод:

Получение всех параметров свойства объекта - ``` Object.getOwnPropertyDescriptor(obj, key); ```.

Проверим работу данного метода:

```javascript
    const obj = {
      get val() {
        return this._val;
      },
      set val(v) {
        this._val = v;
      },
    };

    const desc = Object.getOwnPropertyDescriptor(obj, 'val'); 
    const obj2 = {};
    Object.defineProperty(obj2,'val',desc);
    obj.val = 1;
    obj2.val = 2;
    console.log(obj.val,  obj._val); // 1 1 
    console.log(obj2.val, obj2._val); //2 2 
```

Тесты успешны. Еще отметим, что desc параметры свойства, такие как:
```javascript 
      value
      writable
      enumerable
      configurable
``` 
Также он может содержать ссылки на getter/setter свойства, и в этом случае в нем не будет параметра writable.

Дополнительно отметим следующий моменты: 
```javascript
  console.log(desc);
  // {enumerable: true, configurable: true, get: ƒ, set: ƒ}
  //   configurable: true
  //   enumerable: true
  //   get: ƒ val()
  //   set: ƒ val(v)
  //   __proto__: Object

  console.log(typeof val); // number

```
в параметрах *val* нет *value*


Кажется, что достаточно просто произвести копирование, но уже по другому методу в объект *_data*, и на него поставить getter из корня smart-объекта, но это не совсем так.

Проведем мысленный эксперимент:

Допустим callback-функция зависит от свойств propK, propN, ...
В свою очередь propK зависит от prop1

```
            -----------    ---------- 
            | get call |  |  _data  |
            | ---------|  | --------| 
            |   prop1  |  | prop1   |<-----------|
   clb call |   prop2  |  | prop2   |            |
            |   ...    |  | ...     |            |
            |   propK  |--| propK   |--get call --
            |   ...    |  | ...     |
            |   propN  |--| propN   |
            ------------  ----------|
         
```

То есть при getter smart-объекта зафиксируют обращение к свойствам propK, propN, ..., но не prop1.

Следовательно, изменение prop1 никак не повлияют на механизм ловушек обращений.

Перерисуем схему иначе:

```
             ------------                ------------   
             | get call |               | _data    |
             | ---------|               | ---------|      
             |          |-----get------>|          |    
             |   prop1  |<-----------|  |   prop1  |    
    clb call |----------|            |  |----------|  
             |   prop2  |            |  |   prop2  |      
             |   ...    |            |  |   ...    |      
             |   propK  |--get call -|  |   propK  |    
             |   ...    |               |   ...    |      
             |   propN  |--get--------->|   propN  |    
             ------------               ------------      
         
```

Тогда изменения изменения будут зафиксированны. 


**Идея:**
1. Обход всех полей объекта через Object.getOwnPropertyNames().
2. Копирование через Object.getOwnPropertyDescriptor()
2. Если свойство имеет getter/setter, вставляем их вызов непосредственно в smart-объект.


**Решение:**

Особенности решения:

1. Здесь вместо Object.getOwnPropertyNames() используется Reflect.ownKeys(obj)
2. Вместо Object.defineProperty() используется Reflect.defineProperty();
4. Вместо Object.getOwnPropertyDescriptor() используется Reflect.getOwnPropertyDescriptor();


```javascript
    function createSmartObject(obj) {
      // deleted CODE for space. see previos examples
      const newObj = { _data: {}, _getterCatch: new Set(), _setterMem: new Set() };

      Reflect.ownKeys(obj).forEach(key => {
        const desc = Reflect.getOwnPropertyDescriptor(obj, key);
        if (Reflect.has(desc, "get")) Reflect.defineProperty(newObj, key, desc);
        else {
          Reflect.defineProperty(newObj._data, key, desc);
          Reflect.defineProperty(newObj, key, {
            get() {
              this._getterCatch.add(key);
              return this._data[key];
            },
            set(val) {
              this._data[key] = val;
              const toDoFunc = [];
              this._setterMem.forEach(sCatch => {
                if (sCatch.key === key) {
                  toDoFunc.push(sCatch.wrapClb);
                }
              });
              toDoFunc.forEach(f => f());
            },
          });
        }
      });
    
      return newObj;
    }
    
    function defineComputedField(obj, computedName, clb) {
      // deleted CODE for space. see previos examples
    
      const wrapClb = () => {
        obj._getterCatch.clear();
        obj._data[computedName] = clb(obj);
        obj._setterMem.forEach(wp => {
          if (wp.wrapClb === wrapClb) obj._setterMem.delete(wp);
        });
        obj._getterCatch.forEach(key => obj._setterMem.add({ key, wrapClb }));
      };

      Reflect.defineProperty(obj, computedName, {
        get() {
          return this._data[computedName];
        },
        set() {
          throw new Error("Assignment to computed property");
        },
      });
    
      wrapClb();
    }
```


Проверим тестами:
```javascript
    const oldObj = {
      a: "a",
      b: "test b; ",
      c: " !NothingHappend! ",
      get g() {
        return this._g + this.c;
      },
      set g(val) {
        this._g = val;
      }
    };
    
    oldObj.g = "bad test;";
    
    const obj = createSmartObject(oldObj);
    
    defineComputedField(obj, "abg", data => {
      console.log("called abg");
      return data.a + data.b + data.g;
    });
    //called abg
    
    obj.g = "test getter; ";
    //called abg
    obj.a = "test a; ";
    //called abg
    obj.c = "good job; ";
    //called abg
    console.log(obj.abg);
    //test a; test b; test getter; good job; 
``` 

Тесты подтверждают правильность идеи.

Однако:

**Контрпример:**

Посмотрим, как алгоритм справится с зависимостями между самими вычисляемыми полями
```javascript
const oldObj = { a: 1, b: 1, c: 1, ifComp: 0 };
const o = createSmartObject(oldObj);

defineComputedField(o, "comp1", data => {
  console.log("called 1");
  if (data.ifComp === 1) return data.comp3;
  return data.a + data.b;
});
// called 1

defineComputedField(o, "comp2", data => {
  console.log("called 2");

  return data.comp1 + data.c;
});
// called 2

defineComputedField(o, "comp3", data => {
  console.log("called 3");
  if (data.ifComp === 1) return 2;
  return data.comp2;
});
// called 3
console.log(o.comp1, "exp 2"); // 2 "exp 2"
console.log(o.comp2, "exp 3"); // 3 "exp 3"
console.log(o.comp3, "exp 3"); // 3 "exp 3"
console.log("---------");
o.ifComp = 1;
// ---------
// called 1
// called 3

console.log(o.comp1, "exp 2"); // 3 "exp 2"
console.log(o.comp2, "exp 3"); // 3 "exp 3"
console.log(o.comp3, "exp 2"); // 2 "exp 2"

```

Как мы видим, зависимости не были обработаны должным образом. Дело в том, что при изменении вычисляемого свойства информация об этом событии не идет дальше по цепочкам свойств. Необходимо разработать механизм, который бы выстроил эту связь.


Разрешение зависимостей вычисляемых свойств друг от друга.
---------------------------

**Исследование:**

Рассмотрим существующую схему.
Есть поле cName1 с callback-функцией clb которая зависит от следующих свойств показанными get запросами к полям объекта.
При этом изменение поля cName1 в _data не вызывает никаких событий, поэтому изменения которые коснуться cName1 не вызовут пересчет cName2

```
             ------------               ------------   
             | get call |               | _data    |
             | ---------|               | ---------|      
             |          |-----get------>|          |    
             |   prop1  |<-----------|  |   prop1  |    
    clb call |----------|            |  |----------|  
             |   prop2  |            |  |   prop2  |      
             |----------|            |  |----------|
             |  ...     |            |  |   ...    |     
             |----------|            |  |----------|
             |   propK  |--get call -|  |   propK  |    
             |----------|               |          |
             |   ...    |               |   ...    |      
             |          |               |          |
             |   propN  |--get--------->|   propN  |    
             |          |               |          |
             |----------|               |--------- |
             |          |-----get------>|          |
             |          |               |          |
             |  cName1  |----|          | cName1   |<--|
             |__________|    |          |----------|   |
                             X                         |
             |----------|    |                         |
             |          |   depends, but not react     |               
             |  cName2  |<---|                         |
             |----------|                              |
                                                       |
                                                       |
   cName1=clb---------------set------------------------|

         
```

По своей сути вычисляемые поля не отличаются от обычных полей объекта, которым были назначены пары getter/setter. Поэтому попробуем организовать их поведение таким же образом, а именно:

                                                      
```
             ------------               ------------   
             | get call |               | _data    |
             | ---------|               |  --------|      
             |          |-------------->|          |    
             |   prop1  |<-----------|  |   prop1  |    
    clb call |----------|            |  |----------|  
       |     |   prop2  |            |  |   prop2  |      
       |     |----------|            |  |----------|
       |     |  ...     |            |  |   ...    |     
       |     |----------|            |  |----------|
       |     |   propK  |--get ------|  |   propK  |    
       |     |----------|               |          |
       |     |   ...    |               |   ...    |      
       |     |          |               |          |
       |     |   propN  |--get--------->|   propN  |    
       |     |          |               |          |
       |     |----------|               |--------- |
       |                                |          | 
       |                            |-->|          | 
       |---set---|                  |   | cName1   | 
                 |                  |   ------------ 
                 v                  |   |          |
             |----------|           |   |  ....    |
             |          |-set/get---|      
             |  _cName1 |           
             |  getter  |           
             |  setter  |--if set|                   
             |----------|        |                     
               A                 |
               |                call
               |                 |
               |                 |
               |                 |             |----------|  
               |                 |             | get call |  
               |             clb for cName2 => | ---------|  
               |                               |          |
               |                               |  ....    |
               |                               |          |
                ----------get------------------|  _cName1 |
                                               |          |
                                                 ....

```


Данная схема показывает, что необходимо создать такой же getter/setter для вычислимого поля как и для основных полей в объекте.

**Идея:**

1. Для вычисляемого поля создать абсолютно такой же getter/setter как и для всех остальных полей объекта, с новым названием, к примеру computedNameGS.
2. Для свойства computedName создать только getter на поле computedNameGS.


**Решение:**

Особенности решения:

Необходимо сделать рефакторинг кода а именно:

1. Выделение внутри createSmartObject отдельной сущности - функции по созданию свойств _createGetterSetter.
2. Вызов из defineComputedField ._createGetterSetter(computedNameGS) для создания поля computedNameGS.
3. Примечательно, что поле computedName служит всего лишь интерфейсом с пользователем, явно запрещая производить изменения.

Листинг
```javascript

    function createSmartObject(obj) {
      // deleted CODE for space. see previos examples
      const newObj = {
        _data: {},
        _getterCatch: new Set(),
        _setterMem: new Set()
      };
    
      newObj._createGetterSetter = function(key) { // определение поля getter/setter
        Reflect.defineProperty(this, key, {
          get() {
            this._getterCatch.add(key);
            return this._data[key];
          },
          set(val) {
            this._data[key] = val;
            const toDoFunc = [];
            this._setterMem.forEach(sCatch => {
              if (sCatch.key === key) {
                toDoFunc.push(sCatch.wrapClb);
              }
            });
            toDoFunc.forEach(f => f());
          },
        });
      };
    
      Reflect.ownKeys(obj).forEach(key => {
        const desc = Reflect.getOwnPropertyDescriptor(obj, key);
        if (Reflect.has(desc, "get")) Reflect.defineProperty(newObj, key, desc);
        else {
          Reflect.defineProperty(newObj._data, key, desc);
          newObj._createGetterSetter(key);
        }
      });
    
      return newObj;
    }

    function defineComputedField(obj, computedName, clb) {
      // deleted CODE for space. see previos examples
      const computedNameGS = "_" + computedName;
    
      obj._createGetterSetter(computedNameGS);
    
      const wrapClb = () => {
        obj._getterCatch.clear();
        const result = clb(obj); // пока только вычисляем совйство для того, что бы отловить getter setter и не запустить рекурсию по изменению
        obj._setterMem.forEach(wp => {
          if (wp.wrapClb === wrapClb) obj._setterMem.delete(wp);
        });
        obj._getterCatch.forEach(key => obj._setterMem.add({ key, wrapClb }));
        obj[computedNameGS] = result; //производим присваивание
      };
    
      Reflect.defineProperty(obj, computedName, {
        get() {
          return this[computedNameGS]; //ссылка на новый getter/setter 
        },
        set() {
          throw new Error("Assignment to computed property");
        },
      });
    
      wrapClb();
    }
    
```


Тест на сложные зависимости:

```javascript
    const oldObj = { a: 1, b: 1, c: 1, ifComp: 0 };
      const o = createSmartObject(oldObj);
    
      defineComputedField(o, "comp1", data => {
        console.log("called 1");
        if (data.ifComp === 1) return data.comp3;
        return data.a + data.b;
      });
      // called 1
    
      defineComputedField(o, "comp2", data => {
        console.log("called 2");
    
        return data.comp1 + data.c;
      });
      // called 2
    
      defineComputedField(o, "comp3", data => {
        console.log("called 3");
        if (data.ifComp === 1) return 2;
        return data.comp2;
      });
      // called 3
    
      console.log(o.comp1, "exp 2"); // 2 "exp 2"
      console.log(o.comp2, "exp 3"); // 3 "exp 3"
      console.log(o.comp3, "exp 3"); // 3 "exp 3"
      console.log("---------");
      o.ifComp = 1;
      // ---------
      // called 1
      // called 2
      // called 3
      // called 1
      // called 2
      // called 3
      // called 1
      // called 2
    
      console.log(o.comp1, "exp 2"); // 2 "exp 2"
      console.log(o.comp2, "exp 3"); // 3 "exp 3"
      console.log(o.comp3, "exp 2"); // 2 "exp 2"

```
Результат довольно интересный. поля вычислены верно, но количество вызовов смущает.

Попробуем разобраться

 *called 1*  связка **ifComp - comp1**

 *called 2*  связка **comp1 - comp2**
 
 *called 3*  связка **comp2 - comp3**
 
 *called 1* связка **comp3 - comp1**
 
 *called 2* связка **comp1 - comp2**
 
 *called 3* связка **ifComp - comp3**

 *called 1* связка **comp3 - comp1**
 
 *called 2* связка **comp1 - comp2**

 Осталось выяснить, возможно ли упрощение при таком последовательном вызове.




Оптимизация разрешения зависимостей вычисляемых свойств друг от друга.
---------------------------

**Исследование:**

В предыдущем примере при вычислении полей происходит множество последовательных вызовов функций.
Опишем следующий код:

```javascript 
      const oldObj = { a: 1, b: 1, c: 1, ifComp: 0 };
      const o = createSmartObject(oldObj);
      defineComputedField(o, "comp1", data => {
        console.log("called 1");
        if (data.ifComp === 1) return data.comp3;
        return data.a + data.b;
      });
      defineComputedField(o, "comp2", data => {
        console.log("called 2");
    
        return data.comp1 + data.c;
      });
      defineComputedField(o, "comp3", data => {
        console.log("called 3");
        if (data.ifComp === 1) return 2;
        return data.comp2;
      });
```       

в виде псевдокода:

o = { a, b, c, i } - исходный объект
c1, c2, c3 - вычисляемые поля
f1, f2, f3 - callback-функции

После первого вычисления имеем следующие зависимости
```
    с1 = f1(i, a, b)
    с2 = f2(c1, c) 
    с3 = f3(i, c2)
``` 
Попробуем мысленно проследить возможную динамику изменения в отрыве от уже написанной программы.

```
    с1 = f1(i, a, b)  |          | 
    с2 = f2(c1, c)    |-- 1->i  -| 1. call f1->c1 => f1(i, c3) => call f2->c2 => f2(c1, c) 
    с3 = f3(i, c2)    |          | 2. call f3->c3 => f3(i)   => call f1->c1 => f1(i, c3) => call f2->c2 f2(c1, c) 
```                             

Сосчитаем вызовы:

1. **call f1**
1. **call f2**
1. **call f3**
1. **call f1**
1. **call f2**

Всего пять, а в результатах вывода программы их **8**. Необходимо предотвратить лишние вызовы.

Можно воспользоваться отладчиком кода для поиска возможного дублирования, но чтобы лучше понимать какие именно функции куда передаются, нужно писать новые флаги и новый код, а потом анализировать.

Попробуем определить источник проблем, анализируя сам код и идеи, которые в нем заложены.

Но сначала сформулируем проблематику:

* Код возвращает правильный результат, но вычисления дублируются.

В функции wrapClb, которая объявлена в defineComputedField, происходит исполнение callback-функции, отслеживание вызовов getter и присваивание нового значения аргументу. 

Перейдем в функцию createSmartObject и скроем все лишнее, оставив и прокомментировав то, что влияет на логику работы программы:
```javascript
    function createSmartObject(obj) {
      // ......
      newObj._createGetterSetter = function(key) { 
        Reflect.defineProperty(this, key, {
          get() { /* ..... */  },
          set(val) {
            this._data[key] = val; // присваиваем новое значение
            const toDoFunc = [];   // объявляем массив обернутых callback-функций, которые необходимо вызвать
            this._setterMem.forEach(sCatch => { // заполняем этот массив
                                                 // нельзя вызвать функции сразу, потому что это может привести к бесконечному циклу
              if (sCatch.key === key) {
                toDoFunc.push(sCatch.wrapClb);  // само заполнение 
              }
            });
            toDoFunc.forEach(f => f()); // выполнение функций
          },
        });
      };
    
      // ...........
      return newObj;
    }
``` 

Единственный фрагмент кода, который может быть связан с "дублированием" вызова функций, - это заполнение массива и его последующий вызов.

Давайте мысленно представим механизм работы этого фрагмента кода:

```
    
                               set i=1
    с1=f1(i,a,b)  |          |   |
    с2=f2(c1,c)   |-- 1->i  -|   V
    с3=f3(i,c2)   |          | (toDoFunc[f1,f3]) f1(i, c3)
                                 |                             
                                 |--wrapClb_f2(toDoFunc[f2]) f2(c1,с)
                                 |                |
                                 |                -->wrapClb_f3(toDoFunc[f1]) f3(i)
                                 |                         |
                                 |                         ------>wrapClb_f1(toDoFunc[f2]) f1(i, c3)
                                 |                                           |
                                 |                                           ------>wrapClb_f2(toDoFunc[]) f2(c1, с) 
                                 |
                                 --------------- wrapClb_f3(toDoFunc[f1])
                                                              |
                                                               ------>wrapClb_f1(toDoFunc[f2]) f1(i, c3)
                                                                                    |
                                                                                     ------>wrapClb_f2(toDoFunc[]) f2(c1, с) 
``` 


Очевидно, что ветви дублируются, а именно:
```
   -->wrapClb_f3(toDoFunc[f1]) f3(i)
           | 
           |--> wrapClb_f1(toDoFunc[f2]) f1(i, c3)
                       |
                       ------>wrapClb_f2(toDoFunc[]) f2(c1, с) 
```


Можно заметить, что все зависит от поведения массива toDoFunc, который является локальным для каждой операции. Только представим, что мы изменили свойство и хотим в будущем вызвать callback-функции, но если бы мы знали, что другая ветка тоже собирается ее вызвать. Может быть нам стоит объединить хранилище того, какие callback-функции стоит выполнить.

**Идея:**

1. Сделать toDoFunc свойством нового объекта и в нем хранить список необходимых вызовов wrapClb-функций. При этом сделать не массивом, а Set для автоматического исключения повторений.
2. Перед вызовом wrapClb-функции удалять ее из списка toDoFunc
 

Листинг:
```javascript
function createSmartObject(obj) {
  // deleted CODE for space. see previos examples
  const newObj = {
    _data: {},
    _getterCatch: new Set(),
    _setterMem: new Set(),
    _toDoFunc: new Set()
  };

  newObj._createGetterSetter = function(key) {
    // определение поля getter/setter
    Reflect.defineProperty(this, key, {
      get() {
        this._getterCatch.add(key);
        return this._data[key];
      },
      set(val) {
        this._data[key] = val;
        this._setterMem.forEach(sCatch => {
          if (sCatch.key === key) {
            this._toDoFunc.add(sCatch.wrapClb);
          }
        });
        this._toDoFunc.forEach(f => {
          this._toDoFunc.delete(f); 
          f(); // run wrapClb 
        });
      }
    });
  };

  Reflect.ownKeys(obj).forEach(key => {
    const desc = Reflect.getOwnPropertyDescriptor(obj, key);
    if (Reflect.has(desc, "get")) Reflect.defineProperty(newObj, key, desc);
    else {
      Reflect.defineProperty(newObj._data, key, desc);
      newObj._createGetterSetter(key);
    }
  });

  return newObj;
}

function defineComputedField(obj, computedName, clb) {
  // deleted CODE for space. see previos examples
  const computedNameGS = "_" + computedName;

  obj._createGetterSetter(computedNameGS);

  const wrapClb = () => {
    obj._getterCatch.clear();
    const result = clb(obj);
    obj._setterMem.forEach(wp => {
      if (wp.wrapClb === wrapClb) obj._setterMem.delete(wp);
    });
    obj._getterCatch.forEach(key => obj._setterMem.add({ key, wrapClb }));
    obj[computedNameGS] = result;
  };

  Reflect.defineProperty(obj, computedName, {
    get() {
      return this[computedNameGS];
    },
    set() {
      throw new Error("Assignment to computed property");
    }
  });

  wrapClb();
}
```


Тесты:

```javascript
  const oldObj = { a: 1, b: 1, c: 1, ifComp: 0 };
  const o = createSmartObject(oldObj);

  defineComputedField(o, "comp1", data => {
    console.log("called 1");
    if (data.ifComp === 1) return data.comp3;
    return data.a + data.b;
  });
  // called 1

  defineComputedField(o, "comp2", data => {
    console.log("called 2");

    return data.comp1 + data.c;
  });
  // called 2

  defineComputedField(o, "comp3", data => {
    console.log("called 3");
    if (data.ifComp === 1) return 2;
    return data.comp2;
  });
  // called 3
  console.log(o.comp1, "exp 2"); // 2 "exp 2"
  console.log(o.comp2, "exp 3"); // 3 "exp 3"
  console.log(o.comp3, "exp 3"); // 3 "exp 3"
  console.log("---------");
  o.ifComp = 1;
  // called 1
  // called 3
  // called 2
  // called 1
  // called 2
  console.log(o.comp1, "exp 2"); // 2 "exp 2"
  console.log(o.comp2, "exp 3"); // 3 "exp 3"
  console.log(o.comp3, "exp 2"); // 2 "exp 2"
```


Количество вызовов сократилось:

1. called 1
1. called 3
1. called 2
1. called 1
1. called 2

и оно совпадают с расчетным:

1. **call f1**
1. **call f2**
1. **call f3**
1. **call f1**
1. **call f2**

Примечательно, что было изменено всего 3 строки кода.


**Контрпример:**

Контрпримеров будет несколько:

```javascript
      const proto = { a: 5 };
      const oldObj = Object.create(proto);
      const o = createSmartObject(oldObj);
      console.log(oldObj.a); // 5
      console.log(o.a); // undefined
```
Дело в том, что новый объект мы создаем с помощью литерала {}, абсолютно не обращая внимания на то, что он мог наследоваться от другого объекта.

```javascript
    const oldObj = { a: 1 };
    const o = createSmartObject(oldObj);
    console.log(Object.keys(oldObj));
    // ["a"]
    console.log(Object.keys(o));
    // ["_data", "_getterCatch", "_setterMem", "_createGetterSetter"]
```
Абсолютно неприемлемое поведение. Пользователь получает другой объект и видит свойства, которых не было в исходном.

```javascript
   const oldObj = [1, 2, 3, 4, 5, 6];
   const o = createSmartObject(oldObj);
   console.log(oldObj.reduce((a, b) => a + b)); // 21
   console.log(o.reduce((a, b) => a + b));
    //Uncaught TypeError: o.reduce is not a function
```
Вместо массива у нас объект, у которого нет никаких дополнительных функций.


Прототипное наследование и рефакторинг.
--------------

**Исследование:**


Начнем с того, что в самой в функции createSmartObject новый объект создается невзирая на то, что объект может наследоваться от другого объекта.

Следовательно нам нужно:

1. Получить объект родителя с помощью Object.getPrototypeOf(obj).
2. Произвести наследование с помощью Object.create(Prototype).

Код может выглядеть следующим образом 
```javascript 
     const newObj = Object.create(Object.getPrototypeOf(obj));
```

Одной этой строчкой мы избавляем себя от нескольких проблем:

1. Свойства родителя, к которым может идти обращение, будут доступны новому объекту.
2. Если нам передан массив, то на выходе, конечно будет не совсем массив, но очень близкий к нему объект.

Теперь необходимо выработать принцип, по которому будут определяться поля getter/setter:

1. Вполне логично будет оставлять скрытыми свойства, которые были скрыты в исходном объекте.
2. Что же делать с теми, которые были writable = false? Как одним из вариантов это может быть игнорирования set. Либо не создавать вспомогательный getter, а уже в главную ветку ветку прописывать само свойство. Для целей тестирования выберем первый вариант.

Не ошибкой, но неочевидной работой может быть то, что если мы обращаемся к полю объекту много раз, то происходит попытка заполнения хранилища вызовов, хотя эта функциональность нам нужна только во время вызова callback-функции.

На протяжении всей разработки можно было наблюдать перескок между двумя функциями defineComputedField и createSmartObject. Необходимо было помнить что происходит в одной, а что происходит в другой. Хотелось бы как-то структурировать эти переходы и функциональность.

**Идея:**

1. Создаем объект с помощью Object.create(Object.getPrototypeOf(obj)).
2. У свойств, которые раньше были скрыты, getter/setter тоже будет скрыт.
3. Если свойство read only - setter игнорирует присваивание (примечание: это невозможно будет изменить).
4. Изменим логику функций следующим образом:
5. Записывать вызовы getter только при работе callback-функции
6. Создать служебный объект и сделать его скрытым. В нем хранить все функции и переменные.


**createSmartObject**

  1. Функция, оборачивающая callback функцию, + вызов функции по созданию  getter/setter для вычисляемого поля
  2. Функция, создающая getter/setter 
  3. Копирование объекта + вызов функции **1**
  4. Возвращение smart-объекта.

**defineComputedField**

  1. Вызов функции-обертки для callback функции
  2. Создание getter для взаимодействия с пользователем
  4. Вызов обернутой callback-функции

**Решение:**

Листинг:

``` javascript 
    function createSmartObject(obj) {
      // deleted CODE for space. see previos examples
      const newObj = Object.create(Object.getPrototypeOf(obj));
      const data = {};
      const getterCatch = new Set();
      const setterMem = new Set();
      const toDoFunc = new Set();
      let getterCallAdd = false;

      // --------------- Функция оборачивающая callback функцию
      const createComputedFunction = (clb, computedNameGS) => {
        createGetterSetter(newObj, computedNameGS); // создание геттера по умолчанию скрытого
        const wrapClb = () => {
          getterCatch.clear(); 
          getterCallAdd = true; // начать запись
          const result = clb(newObj);
          getterCallAdd = false; // остановить запись
          setterMem.forEach(wp => {
            if (wp.wrapClb === wrapClb) setterMem.delete(wp);
          });
          getterCatch.forEach(key => setterMem.add({ key, wrapClb }));
          newObj[computedNameGS] = result;
        };
        return wrapClb;
      };
      // --------------- Функция создающая getter/setter 
      const createGetterSetter = function(dObj, key, enumOpt = false, write=true) {
        // определение поля getter/setter
        Reflect.defineProperty(dObj, key, {
          get() {
            if (getterCallAdd) getterCatch.add(key);
            return data[key];
          },
          set(val) {
            if (write) { // можно ли записывать свойство
              data[key] = val;
              setterMem.forEach(sCatch => {
                if (sCatch.key === key) {
                  toDoFunc.add(sCatch.wrapClb);
                }
              });
              toDoFunc.forEach(f => {
                toDoFunc.delete(f);
                f();
              });
            } else throw new Error("read only");
          },
          enumerable: enumOpt
        });
      };
      // ---------------Копирование объекта + вызов функции **1.**
      Reflect.ownKeys(obj).forEach(key => {
        const desc = Reflect.getOwnPropertyDescriptor(obj, key);
        if (Reflect.has(desc, "get")) Reflect.defineProperty(newObj, key, desc);
        else {
          Reflect.defineProperty(data, key, desc);
          createGetterSetter(newObj, key, desc.enumerable,desc.writable);
        }
      });
      // --------------Возвращение smart-объекта.
      const smartObj = {
        createComputedFunction, //есть внешнее обращение
        //data,          // данные параметры 
        //getterCatch,   // можно включить в объект
        //setterMem,     // для целей отладки
        //toDoFunc       //
      };
      Object.defineProperty(newObj, "_smartObject", { // по умолчанию скрыт
        value: smartObj
      });
      return newObj;
    }

    function defineComputedField(obj, computedName, clb) {
      // deleted CODE for space. see previos examples
      const computedNameGS = "_" + computedName;
      const wrapClb = obj._smartObject.createComputedFunction(clb, computedNameGS);

      Reflect.defineProperty(obj, computedName, {
        get() {
          return this[computedNameGS];
        },
        set() {
          throw new Error("Assignment to computed property");
        },
        enumerable: true
      });

  wrapClb();
}
```

Выполним тесты:
```javascript 
    const proto = { a: 5 };
    const oldObj = Object.create(proto);
    const o = createSmartObject(oldObj);
    console.log(oldObj.a); // 5
    console.log(o.a);      // 5
```
Наследование прослеживается.

```javascript 
    const ob = {};
    Object.defineProperty(ob, "pi", { value: 3.14159265 });
    const o = createSmartObject(ob);
    o.pi = 1;
    // Uncaught Error: read only
``` 
Ошибка присваивания срабатывает.

```javascript 
    const oldObj = { a: 1 };
    const o = createSmartObject(oldObj);
    console.log(Object.keys(oldObj));
    // ["a"]
    console.log(Object.keys(o));
    // ["a"]
``` 
Показываются только нужные поля.

```javascript 
    const oldObj = [1, 2, 3, 4, 5, 6];
    const o = createSmartObject(oldObj);
    console.log(oldObj.reduce((a, b) => a + b)); // 21
    console.log(o.reduce((a, b) => a + b)); //21 
``` 
Наследование осуществляется корректно, но давайте посмотрим на данный пример внимательнее. Если мы раскроем объект, то увидим:
```
    Array {_smartObject: {…}}
    0:(...)
    1:(...)
    2:(...)
    3:(...)
    4:(...)
    5:(...)
    length:(...)
    _smartObject:{createComputedFunction: ƒ}
    get 0:ƒ get()
    set 0:set(val) 
    ...
    и так далее...
```
То есть на каждый элемент массива поставлен getter/setter

**Контрпример:**

Стоит ли говорить, что контрпримером будет служить объект с древовидной структурой. Следовательно необходимо глубокое копирование. И мы переходим к **третьему** уровню задачи.


Глубокое копирование объекта. Присваивание полям значений типа объект.
----------------

**Исследование:**

Глубокое копирование объекта является нетривиальной в постановке задачей, поэтому на нужно четко представлять схему изначального объекта и конечного.

Допустим есть следующая структура
```
                       OldObj
                         |
                { a  , c ,   d(get/set),  e  }
                  |    |       |          |      
                  |    |       |          |
                  |    |       |      { a , b }
                  |  "str"     |        |   |                                      
                  |     A      |       [2]  |               
     [1 , 2 , 3 , 4]    |------|            |                                 
              A                |           "str"                
              |                |            A                     
              |----------------|------------|                                 
``

Можно сделать аналогично тому, как это делалось в предыдущей задаче. То есть скопировать структуру, а вместо значений подставить геттеры значения, которые можно хранить в подобной структуре, либо где-то еще:

```
                       newObj
                         |
                { a  , G ,   d(get/set),  e  }
                  |    A       |          |      
                  |    |       |          |
                  |    |       |      { a , G }
                  |    |       |        |   A                                      
                  |    |       |       [G]  |               
     [G , G , G , G]   +-------+            |                                 
              A                |            |                    
              |                |            |                     
              |----------------+------------|                                 
``
  
простым контрпримером может служить следующее присвоение:

```
    newObj.e = { a:2 , b:3 };
```

В поле ```newObj.e``` появилась новая ссылка на объект.

Приведем еще примеры:
```
    newObj.a = 1;
```

В поле ``` newObj.a```  появилась, вместо ссылки на массив, значение.

Эти два примера объединяет то, что об этих изменениях согласно предыдущей схеме, мы ничего не будем знать. Нет событий, которые могли бы что-то нам об этом рассказать.

Попробуем нарисовать схему, которая бы соответствовала таким требованиям:
```
                       newObj
                         |
                { G  , G ,   d(get/set)  ,  G  }
                  |    A       |            |      
                  |    |       |            |
                  |    |       |        { G , G }
                  |    |       |          |   A                                      
                  |    |       |         [G]  |               
     [G , G , G , G]   +-------+              |                                 
              A                |              |                    
              |                |              |                     
              |----------------+--------------|                                 

```

То есть каждое поле содержит в себе getter/setter, а уже getter оставшийся от предыдущего объекта будет оставлен как есть.

Попробуем реализовать данный механизм на практике в отдельности от исходной задачи.

Необходимо продумать: 

1. Где будут храниться данные getter/setter, которые ссылаются на объекты.
2. Как производить копирование.


Проведем мысленный эксперимент:

1. Представим объект древовидной структуры.
2. Спускаемся на первый уровень вложенности.
3. Мы знаем, что все эти свойства должны иметь getter/setter.
4. Значит каждому свойству назначаем пару getter/setter. Важный момент: принципиально ли, в каком виде и как будут храниться сами данные, если  пользователь их все равно не видит? Конечно **нет**. Самое главное, что бы на них была ссылка у getter/setter. 
5. Делаем присваивание этому полю значение объекта соответствующего объекта.
6. **Тонкий момент** А если это поле тоже объект, то нужно ему так же сформировать структуру, то есть опуститься ниже и провести абсолютно такие же шаги. Значит, при формировании getter/setter нужно еще раз вызвать функцию клонирования.

Получается рекурсия.


Еще раз повторим
1. Обходим все свойства объекта
2. Каждому свойству создаем getter/setter. В setter если значение объект вызывает процедуру копирования.
3. После создания присваиваем ему значение.

Проверка идеи

```javascript
    function clone(obj, data = []) {
        const isObj = val => typeof val === "object" && val !== null; // проверка на объект
        if (!isObj(obj)) return obj; // если не объект, то возвращаем. Стоит отметить, что согласно этому условию будут скопированы и функции, как ссылки.
    
        const out = Object.create(Object.getPrototypeOf(obj)); // копирование прототипа
        Reflect.ownKeys(obj).forEach(key => { // обходим все собственные свойства
          const smblKey = Symbol();  // создаем уникальный идентификатор
          const descr = Reflect.getOwnPropertyDescriptor(obj, key); // получение свойств
          if (Reflect.has(descr, "get")) Reflect.defineProperty(out, key, descr); // если это getter/setter, то определяем его в новый объект копированием по ссылке
          else
            Reflect.defineProperty(out, key, {
              get() {
                return data[smblKey]; //
              },
              set(val) {
                data[smblKey] = clone(val, data);
                // тут необходимо остановиться
                // 1. data[smblKey] - так как мы используем Symbol, 
                // то можем быть уверены уверены, что в данном объекте( data ) все 
                // названия полей будут (smblKey) будут уникальны, на то он и Symbol 
                // 2. clone(val, data) - здесь как раз отдается val
                //  в функцию clone: если это обычное
                //  значение, то вернутся значение, а если это объект,
                //  то создастся новый с установленными getter setter
              },
              enumerable: descr.enumerable
            });
          out[key] = obj[key];
        });
    
            return out;
      }
    
      oldObj = { a: 1, b: 2, c: { a: 3, b: 4 } };
    
      const o = clone(oldObj);
      o.b = { m: 2, l: 5, s: [1, 1, 1, 1, 1, 1], p: { z: 2, d: 3 } };
      console.log(Reflect.getOwnPropertyDescriptor(o.b.p, "z"));
      //enumerable: false, configurable: false, get: ƒ, set: ƒ}
      o.b = 1;
      console.log(Reflect.getOwnPropertyDescriptor(o, "b"));
      // {enumerable: true, configurable: false, get: ƒ, set: ƒ}
      console.log(o.b); //1
```

Идея оказалась успешной.

Особо отметим применение Symbol – это позволяет не клонировать древовидную структуры для данных, а также облегчить доступ к ним. В процессе создания объекта не приходится думать об уникальности имен либо о вложенности.


**Идея:**

Рекурсивно проходим исходный объект. Каждому свойству назначаем getter/setter, а затем присваиваем ему значение. 


Особенности данного решения: 

1. Использование Symbol для идентификации полей. В тех же самых ловушках для getter нет необходимости придумывать механизм ветвления дерева объекта и его полей для точного и однозначного определения вызова конкретного поля, можно использовать Symbol с объектом data одного уровня вложенности.
2. Изменена логика первоначального присваивания если свойство read only.
3. Добавлено наследование configurable в getter/setter.

**Решение:**

Листинг:

``` javascript 
    function createSmartObject(obj) {
      if (Reflect.has(obj, "_smartObject"))
        throw new Error("Property _smartObject already exists.");
    
      const data = {};
      const getterCatch = new Set();
      const setterMem = new Set();
      const toDoFunc = new Set();
      const iC = []; // для цикличных ссылок
      const oC = []; // для цикличных ссылок
      let getterCallAdd = false;
      let initGetSet = false;
    
      // --------------- Функция оборачивающая callback функцию
      const createComputedFunction = (clb, computedNameGS) => {
        createGetterSetter(newObj, computedNameGS); // создание геттера по умолчанию скрытого
        const wrapClb = () => {
          getterCatch.clear();
          getterCallAdd = true; // начать запись
          const result = clb(newObj);
          getterCallAdd = false; // остановить запись
          setterMem.forEach(wp => {
            if (wp.wrapClb === wrapClb) setterMem.delete(wp);
          });
          getterCatch.forEach(key => setterMem.add({ key, wrapClb }));
          newObj[computedNameGS] = result;
        };
        return wrapClb;
      };
    
      // --------------- Функция создающая getter/setter
      const createGetterSetter = (
        dObj,
        key,
        enumOpt = false,
        symKey = Symbol(),
        write = true,
        config = true
      ) => {
        Reflect.defineProperty(dObj, key, {
          get() {
            if (getterCallAdd) getterCatch.add(symKey);
            return data[symKey];
          },
          set(val) {
            if (write || initGetSet) {
              initGetSet = false;
              data[symKey] = cloneObj(val);
              setterMem.forEach(sCatch => {
                if (sCatch.key === symKey) {
                  toDoFunc.add(sCatch.wrapClb);
                }
              });
              toDoFunc.forEach(f => {
                toDoFunc.delete(f);
                f();
              });
            } else throw new Error("read only");
          },
          enumerable: enumOpt,
          configurable: config
        });
      };
    
      // ---------------Клонирование объекта
      const cloneObj = iObj => {
        const isObj = val => typeof val === "object" && val !== null;
        if (!isObj(iObj)) return iObj;
    
        const idx = iC.findIndex(x => x === iObj); // для возможных цикличных объектов
        if (idx >= 0) return oC[idx];
    
        const oObj = Object.create(Object.getPrototypeOf(iObj));
    
        iC.push(iObj);
        oC.push(oObj);
    
        Reflect.ownKeys(iObj).forEach(key => {
          const desc = Reflect.getOwnPropertyDescriptor(iObj, key);
          if (Reflect.has(desc, "get")) Reflect.defineProperty(oObj, key, desc);
          else {
            const symKey = Symbol(key);
            createGetterSetter(
              oObj,
              key,
              desc.enumerable,
              symKey,
              desc.writable,
              desc.configurable
            );
            initGetSet = true;
            oObj[key] = iObj[key];
          }
        });
    
        return oObj;
      };
            
      // --------------Возвращение smart-объекта.
      const newObj = cloneObj(obj);
    
      const smartObject = {
        data,
        getterCatch,
        setterMem,
        toDoFunc,
        createComputedFunction,
        createGetterSetter
      };
    
      Reflect.defineProperty(newObj, "_smartObject", {
        value: smartObject,
        enumearble: false
      });
      return newObj;
    }
    
    function defineComputedField(obj, computedName, clb) {
      if (Reflect.has(obj, computedName))
        throw new Error(`Property "${computedName}" already exists.`);
      const computedNameGS = Symbol(computedName);
      const wrapClb = obj._smartObject.createComputedFunction(clb, computedNameGS);
    
      Reflect.defineProperty(obj, computedName, {
        get() {
          return this[computedNameGS];
        },
        set() {
          throw new Error("Assignment to computed property");
        },
        enumerable: true
      });
    
      wrapClb();
    }
```

Проведем следующий тест:

```javascript

    const nestedObj = {
      main: {
        changeToPrim: {
          get a() {
            return this._a;
          },
          set a(value) {
            this._a = value;
          },
          b: 2,
          c: 3,
          _a: undefined
        },
        changeToObj: 22,
        ac: 23
      },
      someNum1: 222,
      someNum2: 333
    };
    
    nestedObj.link = nestedObj;

    const smartNestedObj = createSmartObject(nestedObj);
    
    smartNestedObj.main.changeToPrim = 1; // A primitive value is assigned to a property of type 'Object'
    
    const newchangeToObj = {
      get a() {
        return this._a;
      },
      set a(v) {
        this._a = v;
      },
      b: 2,
      c: 3,
      _a: 3
    };
    
    smartNestedObj.main.changeToObj = newchangeToObj; // An object is assigned to a property
    
    smartNestedObj.main.changeToObj.a = 1;
    
    console.log("\nBoth fields are defined and computed");
    
    defineComputedField(smartNestedObj, "sum1", data => {
      console.log("Computed sum1");
      if (data.main.changeToObj.a === 1) {
        return `main.changeToObj.b ${data.main.changeToObj.b} + main.ac ${data.main.ac}`;
      }
      return `main.changeToObj.c ${data.main.changeToObj.c} + someNum2 ${data.someNum2}`;
    });
    
    // Computed sum1
    
    defineComputedField(smartNestedObj, "sum2", data => {
      console.log("Computed sum2");
      if (data.main.changeToObj.a === 1) {
        return `main.changeToPrim ${data.main
          .changeToPrim} + someNum1 ${data.someNum1}`;
      }
      return `main.changeToObj.b ${data.main.changeToObj
        .b} + main.changeToObj.c ${data.main.changeToObj.c}`;
    });
    
    // Computed sum2
    
    console.log(
      "sum1 test: ",
      smartNestedObj.sum1 === "main.changeToObj.b 2 + main.ac 23"
    );
        console.log(
      "sum2 test: ",
      smartNestedObj.sum2 === "main.changeToPrim 1 + someNum1 222"
    );
    
    console.log("\nBoth fields are computed");
    smartNestedObj.main.changeToObj.a = 10; // Computed sum1, Computed sum2
    console.log(
      "sum1 test: ",
      smartNestedObj.sum1 === "main.changeToObj.c 3 + someNum2 333"
    );
    console.log(
      "sum2 test: ",
      smartNestedObj.sum2 === "main.changeToObj.b 2 + main.changeToObj.c 3"
    );
    
    console.log("\nFirst field is computed");
    smartNestedObj.someNum2 = 3330; // Computed sum1
    console.log(
      "sum1 test: ",
      smartNestedObj.sum1 === "main.changeToObj.c 3 + someNum2 3330"
    );
    console.log(
      "sum2 test: ",
      smartNestedObj.sum2 === "main.changeToObj.b 2 + main.changeToObj.c 3"
    );
    
    console.log("\nSecond field is computed");
    smartNestedObj.main.changeToObj.b = 20; // Computed sum2
    console.log(
          "sum1 test: ",
      smartNestedObj.sum1 === "main.changeToObj.c 3 + someNum2 3330"
    );
    console.log(
      "sum2 test: ",
      smartNestedObj.sum2 === "main.changeToObj.b 20 + main.changeToObj.c 3"
    );
    
    console.log("\nNone of the fields is computed");
    smartNestedObj.main.changeToPrim = 2; // Nothing recomputed
    console.log(
      "sum1 test: ",
      smartNestedObj.sum1 === "main.changeToObj.c 3 + someNum2 3330"
    );
    console.log(
      "sum2 test: ",
      smartNestedObj.sum2 === "main.changeToObj.b 20 + main.changeToObj.c 3"
    );
```


Как можно видеть, все тесты пройдены успешно.

```
    Both fields are defined and computed
    Computed sum1
    Computed sum2
    sum1 test:  true
    sum2 test:  true

    Both fields are computed
    Computed sum1
    Computed sum2
    sum1 test:  true
    sum2 test:  true

    First field is computed
    Computed sum1
    sum1 test:  true
    sum2 test:  true

    Second field is computed
    Computed sum2
    sum1 test:  true
    sum2 test:  true

    None of the fields is computed
    sum1 test:  true
    sum2 test:  true
```

Необходимо более полное тестирование. Возможно, в процессе разработки мог потеряться какой-то функционал. 

Тестирование.
--------------

Общий план тестирования:

1. Глубокое копирование объекта
    1. Совпадение значений полей исходного и смарт-объекта
    1. Изначальный объект явлется массивом. Проверка работоспосоности функций массива.
    1. Изначальный объект образован из произвольного объекта-прототипа.
    1. Копирования функций исходного объекта
    1. Копирование свойства с геттером/сеттером (значение присваивается и возвращается)
        1. Произвольное значение-примитив
        1. Null
        1. NaN
        1. Значение-массив (доступно свойство length) 
        1. Массив. Доступны функции reduce, map
    1. Копирование атрибутов свойств
        1. Свойство с нулевым уровнем вложенности
            1. Non-Writable. Значение остается прежним после присваивания
            1. Enumerable
            1. Configurable
        2. Вложенное свойство
            1. Non-Writable. Значение остается прежним после присваивания.
            1. Enumerable
            1. Configurable
1. Добавление вычисляемых полей
    1. Значения вычисляемых полей после добавления вычислены правильно
    1. Пересчитываются только те поля, которые зависят от измененного свойства. Проверяется число вызовов callback-функции
    1. Геттер меняет значения нескольких скрытых полей, зависимые от них вычисляемые поля правильно пересчитываются
1. Присвоение свойству значения-объекта (повторить проверки п. 1 и 2)
    1. После добавления значения-объекта вложенные свойства имеют правильные значения
    1. Копирование свойства с геттером/сеттером (значение присваивается и возвращается)
        1. Произвольное значение-примитив
        1. Null
        1. NaN
        1. Значение-массив (доступно свойство length) 
        1. Массив. Доступны функции reduce, map
    1. Копирование атрибутов свойств при присвоении значения-объекта
        1. Non-Writable. Значение остается прежним после присваивания.
        1. Enumerable
        1. Configurable 
    1. Вычисляемые поля пересчитываются при изменении созданных вложенных свойств (проверки из п.2)
1. Присвоение свойству значения-примитива
    1. Значение присваивается и возвращается
    1. Вычисляемые поля пересчитываются при изменении значения
1. Расчет вычисляемых полей, зависящих от других вычисляемых полей, минимальным количеством вызовов callback-функций.
    1. После задания вычисляемых свойств их значения рассчитаны правильно.
    1. При полном изменении требуемого порядка расчета значения свойств пересчитаны правильно и без лишних вызовов callback-функций.
    1. При пересчете части вычисляемых свойств их значения определены правильно и без лишних вызовов callback-функций. 



Сейчас тесты на стадии написания, **Вы** можете принять в этом активное участие!


Планы по дальнейшему разбору:
----------

1. Прокси.


Благодарности за вклад в создании статьи:
---------------
**@max_shestachenko**

**@FXDuke**

**Alex Kei**



