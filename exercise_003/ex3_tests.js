const tObj = {};

tObj[Symbol()] = () => {
  console.log('Change computed fileds');

  const old = { a: 1 };

  const o = createSmartObject(old);

  defineComputedField(o, 'a', d => {
    return d.a;
  });

  o.cn = 2;

  return d.cn === 1;
};

tObj[Symbol()] = () => {
  console.log('Test for nonwritale field.');
  let passed = true;

  const old = { a: 1 };
  const t = 'asdfasdfa';

  Object.defineProperty(old, 'nonwritable', {
    value: t,
    writable: false,
  });

  const o = createSmartObject(old);

  try {
    o.nonwritable = 1;
  } catch (e) {}
  return o.nonwritable === t;
};

// -----------------

tObj[Symbol()] = () => {
  console.log('Create 3 computed fields. Independent change.');
  let cnt1 = 0;
  let cnt2 = 0;
  let cnt3 = 0;

  const oldObj = { a: 1, b: 2, c: 4, l: 5 };

  const obj = createSmartObject(oldObj);

  defineComputedField(obj, 'ab', d => {
    cnt1 += 1;
    return d.a + d.b;
  });

  defineComputedField(obj, 'ba', d => {
    cnt2 += 1;
    return d.b + d.a;
  });

  defineComputedField(obj, 'cn', d => {
    cnt3 += 1;
    return d.c;
  });

  obj.a = 3;
  obj.c = 4;
  obj.l = 6;
  return (
    cnt1 === 2 &&
    cnt2 === 2 &&
    cnt3 === 2 &&
    obj.a === 3 &&
    obj.c === 4 &&
    obj.cn === 4 &&
    obj.ab === 5 &&
    obj.ba === 5
  );
};

//----------------

tObj[Symbol()] = () => {
  console.log('Correct clone deep array.');

  const d = [1, 5, [1, 2, 3]];

  const d2 = [1, 5, 1, 2, 3];

  const o = createSmartObject(d);

  const o2 = createSmartObject(d2);

  return (
    o[0] === d[0] &&
    o[1] === d[1] &&
    o[2][0] === d[2][0] &&
    o[2][1] === d[2][1] &&
    o[2][2] === d[2][2] &&
    o.length === d.length &&
    o[2].length === d[2].length &&
    o[2].reduce((a, b) => a + b) === d[2].reduce((a, b) => a + b) &&
    o2.reduce((a, b) => a + b) === d2.reduce((a, b) => a + b)
  );
};

// -------------------

tObj[Symbol()] = () => {
  console.log('Correct prototype clone.');

  const d = { a: { b: { l: 1 } } };

  const d2 = Object.create(d);
  d2.z = 4;

  const o = createSmartObject(d2);

  return (
    d2.a === o.a && d2.a.b === o.a.b && d2.a.b.l === o.a.b.l && d2.z === o.z
  );
};

// -------------------

tObj[Symbol()] = () => {
  console.log('Correct clone object with getter/setter. Test computed field.');

  const d = {
    a: 'a',
    b: 'b',
    c: 't',
    get g() {
      return this._g + this.c;
    },
    set g(val) {
      this._g = val;
    },
  };

  d.g = 'g';

  const dCheck = {
    a: 'a',
    b: 'b',
    c: 't',
    get g() {
      return this._g + this.c;
    },
    set g(val) {
      this._g = val;
    },
  };

  dCheck.g = 'g';

  const o = createSmartObject(d);

  let cnt = 0;
  let cntCheck = 0;
  defineComputedField(o, 'test', data => {
    cnt += 1;
    return data.a + data.b + data.g + data._g;
  });
  cntCheck += 1;

  o.a = 6;
  dCheck.a = 6;
  cntCheck += 1;

  o.g = 8;
  dCheck.g = 8;
  cntCheck += 1;

  o._g = 9;
  dCheck._g = 9;
  cntCheck += 1;

  return (
    cntCheck === cnt &&
    o.a === dCheck.a &&
    o.g === dCheck.g &&
    o._g === dCheck._g
  );
};

// eval tests

Object.getOwnPropertySymbols(tObj).forEach(key => {
  'use strict';
  try {
    let m = tObj[key]();
    console.log('   pass: %c' + m, m ? 'color: #00aa00' : 'color: #ff0000');
  } catch (e) {
    console.log('%c  L__Test failed: ' + e.message, 'color: #ff0000');
  }
});
