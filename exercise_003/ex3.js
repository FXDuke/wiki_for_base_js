function createSmartObject(obj) {
  if (Reflect.has(obj, '_smartObject'))
    throw new Error('Property _smartObject already exists.');

  const data = {};
  const getterCatch = new Set();
  const setterMem = new Set();
  const toDoFunc = new Set();
  const iC = []; // для цикличных ссылок
  const oC = []; // для цикличных ссылок
  let getterCallAdd = false;
  let initGetSet = false;

  // --------------- Функция оборачивающая callback функцию
  const createComputedFunction = (clb, computedNameGS) => {
    createGetterSetter(newObj, computedNameGS); // создание геттера по умолчанию скрытого
    const wrapClb = () => {
      getterCatch.clear();
      getterCallAdd = true; // начать запись
      const result = clb(newObj);
      getterCallAdd = false; // остановить запись
      setterMem.forEach(wp => {
        if (wp.wrapClb === wrapClb) setterMem.delete(wp);
      });
      getterCatch.forEach(key => setterMem.add({ key, wrapClb }));
      newObj[computedNameGS] = result;
    };
    return wrapClb;
  };

  // --------------- Функция создающая getter/setter
  const createGetterSetter = (
    dObj,
    key,
    enumOpt = false,
    symKey = Symbol(),
    write = true,
    config = true,
  ) => {
    Reflect.defineProperty(dObj, key, {
      get() {
        if (getterCallAdd) getterCatch.add(symKey);
        return data[symKey];
      },
      set(val) {
        if (write || initGetSet) {
          initGetSet = false;
          data[symKey] = cloneObj(val);
          setterMem.forEach(sCatch => {
            if (sCatch.key === symKey) {
              toDoFunc.add(sCatch.wrapClb);
            }
          });
          toDoFunc.forEach(f => {
            toDoFunc.delete(f);
            f();
          });
        } else throw new Error('read only');
      },
      enumerable: enumOpt,
      configurable: config,
    });
  };

  // ---------------Клонирование объекта
  const cloneObj = iObj => {
    const isObj = val => typeof val === 'object' && val !== null;
    if (!isObj(iObj)) return iObj;

    const idx = iC.findIndex(x => x === iObj); // для возможных цикличных объектов
    if (idx >= 0) return oC[idx];

    const oObj = Object.create(Object.getPrototypeOf(iObj));

    iC.push(iObj);
    oC.push(oObj);

    Reflect.ownKeys(iObj).forEach(key => {
      const desc = Reflect.getOwnPropertyDescriptor(iObj, key);
      if (Reflect.has(desc, 'get')) Reflect.defineProperty(oObj, key, desc);
      else {
        const symKey = Symbol(key);
        createGetterSetter(
          oObj,
          key,
          desc.enumerable,
          symKey,
          desc.writable,
          desc.configurable,
        );
        initGetSet = true;
        oObj[key] = iObj[key];
      }
    });

    return oObj;
  };

  // --------------Возвращение smart-объекта.
  const newObj = cloneObj(obj);

  const smartObject = {
    data,
    getterCatch,
    setterMem,
    toDoFunc,
    createComputedFunction,
    createGetterSetter,
  };

  Reflect.defineProperty(newObj, '_smartObject', {
    value: smartObject,
    enumearble: false,
  });
  return newObj;
}

function defineComputedField(obj, computedName, clb) {
  if (Reflect.has(obj, computedName))
    throw new Error(`Property "${computedName}" already exists.`);
  const computedNameGS = Symbol(computedName);
  const wrapClb = obj._smartObject.createComputedFunction(clb, computedNameGS);

  Reflect.defineProperty(obj, computedName, {
    get() {
      return this[computedNameGS];
    },
    set() {
      throw new Error('Assignment to computed property');
    },
    enumerable: true,
  });

  wrapClb();
}
